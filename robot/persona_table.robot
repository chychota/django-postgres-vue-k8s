*** Settings ***
Documentation     Tests persona table on Settings page.
Library           Collections
Library           String
Library           SeleniumLibrary
Library           RequestsLibrary
Library           PortalLibrary/BasicAuth.py
Suite Setup       Setup
Suite Teardown    Teardown

*** Variables ***
${SERVER}         localhost
${PROTOCOL}       https
${BROWSER}        Firefox
${DELAY}          0
${HOME URL}       ${PROTOCOL}://${SERVER}/
${HOME URL NO SLASH}    ${PROTOCOL}://${SERVER}
${LOGIN URL}      ${PROTOCOL}://${SERVER}/auth/sign-in
${SETTINGS URL}   ${PROTOCOL}://${SERVER}/settings
${USERNAME}       placeholder
${AUTH TOKEN}     placeholder
${VALID PASSWORD}   admin

*** Keywords ***
Setup
    Log     Browser: ${BROWSER}                 INFO    console=True
    Log     Browser options: ${BROWSER OPTS}    INFO    console=True
    Log     Server: ${SERVER}                   INFO    console=True
    Set Selenium Speed    ${DELAY}
    Create Dummy User
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Login To Portal
    Open Browser To Settings Page

Teardown
    Close Browser

Create Dummy User
    ${temp username}=    Generate Random String      8   [LETTERS][NUMBERS]
    Set Suite Variable     ${USERNAME}    ${temp username}
    Log     Dummy user will have ${USERNAME} as both their username and password    INFO    console=True
    Send Register Request   ${USERNAME}   ${USERNAME}

Send Register Request
    [Arguments]     ${username}     ${password}
    ${basic auth}=      Generate Basic Auth     admin       ${VALID PASSWORD}
    Log     ${basic auth}     INFO    console=True
    Create session      portal      ${HOME URL NO SLASH}
    &{data}=        Create dictionary   username=${username}  password=${password}  email=dummy@nokia.com
    ${headers}=     Create dictionary   Authorization=${basic auth}
    ${resp}=    Post request    portal      /api/auth/register/     json=${data}    headers=${headers}
    Log     ${resp}     INFO    console=True
    Log     ${resp.json()}     INFO    console=True
    Status Should Be    200     ${resp}
    Dictionary Should Contain Key   ${resp.json()}      token
    ${temp auth token}=      Get From Dictionary    ${resp.json()}   token
    Set Suite Variable      ${AUTH TOKEN}   ${temp auth token}
    Log     Auth token is now ${AUTH TOKEN}   INFO    console=True

Login To Portal
    Go To    ${LOGIN URL}
    Input Text    username    ${username}
    Input Text    password    ${username}
    Click Button    sign-in-btn
    Wait Until Location Is      ${HOME URL}     2

Open Browser To Settings Page
    # Go to homepage to force a refresh of setting page
    Go To   ${HOME URL}
    Go To   ${SETTINGS URL}

*** Test Cases ***
If User Has No Personas Then Create First Persona Button Is Visible
    Wait Until Element Is Visible   create-first-persona-btn   5
