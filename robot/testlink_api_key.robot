*** Settings ***
Documentation     Tests adding and updating TestLink API key on settings page.
Library           Collections
Library           String
Library           SeleniumLibrary
Library           RequestsLibrary
Library           PortalLibrary/BasicAuth.py
Suite Setup       Setup
Suite Teardown    Teardown

*** Variables ***
${SERVER}         localhost
${PROTOCOL}       https
${BROWSER}        Firefox
${DELAY}          0
${HOME URL}       ${PROTOCOL}://${SERVER}/
${HOME URL NO SLASH}    ${PROTOCOL}://${SERVER}
${LOGIN URL}      ${PROTOCOL}://${SERVER}/auth/sign-in
${SETTINGS URL}   ${PROTOCOL}://${SERVER}/settings
${USERNAME}       placeholder
${AUTH TOKEN}     placeholder

*** Keywords ***
Setup
    Log     Browser: ${BROWSER}                 INFO    console=True
    Log     Browser options: ${BROWSER OPTS}    INFO    console=True
    Log     Server: ${SERVER}                   INFO    console=True
    Set Selenium Speed    ${DELAY}
    Create Dummy User
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Login To Portal
    Open Browser To Settings Page

Teardown
    Close Browser
    Remove TestLink API Key Via REST Request

Create Dummy User
    ${temp username}=    Generate Random String      8   [LETTERS][NUMBERS]
    Set Suite Variable     ${USERNAME}    ${temp username}
    Log     Dummy user will have ${USERNAME} as both their username and password    INFO    console=True
    Send Register Request   ${USERNAME}   ${USERNAME}

Send Register Request
    [Arguments]     ${username}     ${password}
    ${basic auth}=      Generate Basic Auth     admin       ${VALID PASSWORD}
    Log     ${basic auth}     INFO    console=True
    Create session      portal      ${HOME URL NO SLASH}
    &{data}=        Create dictionary   username=${username}  password=${password}  email=dummy@nokia.com
    ${headers}=     Create dictionary   Authorization=${basic auth}
    ${resp}=    Post request    portal      /api/auth/register/     json=${data}    headers=${headers}
    Log     ${resp}     INFO    console=True
    Log     ${resp.json()}     INFO    console=True
    Status Should Be    200     ${resp}
    Dictionary Should Contain Key   ${resp.json()}      token
    ${temp auth token}=      Get From Dictionary    ${resp.json()}   token
    Set Suite Variable      ${AUTH TOKEN}   ${temp auth token}
    Log     Auth token is now ${AUTH TOKEN}   INFO    console=True

Login To Portal
    Go To    ${LOGIN URL}
    Input Text    username    ${username}
    Input Text    password    ${username}
    Click Button    sign-in-btn
    Wait Until Location Is      ${HOME URL}     2

Open Browser To Settings Page
    # Go to homepage to force a refresh of setting page
    Go To   ${HOME URL}
    Go To   ${SETTINGS URL}

Remove TestLink API Key Via REST Request
    Log     Username is ${USERNAME}   INFO    console=True
    Log     Auth token is ${AUTH TOKEN}   INFO    console=True
    ${headers}=     Create Dictionary   Authorization=Token ${AUTH TOKEN}
    Log     ${headers}      INFO    console=True
    Create session      portal      ${HOME URL NO SLASH}
    ${resp}=    Delete Request   portal      /api/v1/user/testlink-api-key/     headers=${headers}
    Log     ${resp}     INFO    console=True
    Log     ${resp.json()}     INFO    console=True

Submit TestLink API Key
    [Arguments]     ${api key}
    Location Should Be    ${SETTINGS URL}
    Wait Until Element Is Visible   testlink-key-input   5
    Input Text      testlink-key-input      ${api key}
    Click Button    testlink-key-submit-btn

*** Test Cases ***
Can Submit TestLink API Key
    Remove TestLink API Key Via REST Request
    Open Browser To Settings Page
    ${TESTLINK KEY}=    Generate Random String      32   [LETTERS][NUMBERS]
    Log     TestLink key generated as ${TESTLINK KEY}   INFO    console=True
    Submit TestLink API Key     ${TESTLINK KEY}
    Wait Until Element Is Visible   dialog  5
    Element Text Should Be      dialog-msg      Your TestLink API key has been set to "${TESTLINK KEY}".

Receive Error Message If API Key Has Non-alphanumeric Characters
    Remove TestLink API Key Via REST Request
    Open Browser To Settings Page
    Submit TestLink API Key     adflkjf+
    Wait Until Element Is Visible   dialog  5
    Element Text Should Be      dialog-msg      TestLink API key must not be an empty string or contain any non-alphanumeric characters.

Receive Error Message If API Key Is Empty
    Remove TestLink API Key Via REST Request
    Open Browser To Settings Page
    Submit TestLink API Key     ${EMPTY}
    Wait Until Element Is Visible   dialog  5
    Element Text Should Be      dialog-msg      TestLink API key must not be an empty string or contain any non-alphanumeric characters.

Receive Error Message If API Key Is Incorrect Length
    Remove TestLink API Key Via REST Request
    Open Browser To Settings Page
    ${TESTLINK KEY}=    Generate Random String      16   [LETTERS][NUMBERS]
    Submit TestLink API Key     ${TESTLINK KEY}
    Wait Until Element Is Visible   dialog  5
    Element Text Should Be      dialog-msg      Error: Key must be a string of length 32

Can View TestLink API Key
    Remove TestLink API Key Via REST Request
    Open Browser To Settings Page
    ${TESTLINK KEY}=    Generate Random String      32   [LETTERS][NUMBERS]
    Log     TestLink key generated as ${TESTLINK KEY}   INFO    console=True
    Submit TestLink API Key     ${TESTLINK KEY}
    Wait Until Element Is Visible   dialog  5
    Element Text Should Be      dialog-msg      Your TestLink API key has been set to "${TESTLINK KEY}".
    Open Browser To Settings Page
    Wait Until Element Is Visible   testlink-key-display-btn    5
    Click Button    testlink-key-display-btn
    Sleep   2
    Element Text Should Be  testlink-key-lbl    ${TESTLINK KEY}

Can Delete TestLink API Key
    Remove TestLink API Key Via REST Request
    Open Browser To Settings Page
    ${TESTLINK KEY}=    Generate Random String      32   [LETTERS][NUMBERS]
    Log     TestLink key generated as ${TESTLINK KEY}   INFO    console=True
    Submit TestLink API Key     ${TESTLINK KEY}
    Wait Until Element Is Visible   dialog  5
    Open Browser To Settings Page
    Wait Until Element Is Visible   testlink-key-delete-btn    5
    Click Button    testlink-key-delete-btn
    Wait Until Element Is Visible   dialog  5
    Element Text Should Be      dialog-msg      Your TestLink API key has been deleted.
