*** Settings ***
Documentation     Tests for Edit Job page.
Library           SeleniumLibrary
Suite Setup       Open Browser To Login Page
Suite Teardown    Close Browser

*** Variables ***
${SERVER}         localhost
${PROTOCOL}       https
${BROWSER}        Firefox
${DELAY}          0
${VALID USER}     admin
${VALID PASSWORD}    admin
${LOGIN URL}      ${PROTOCOL}://${SERVER}/auth/sign-in
${HOME URL}    ${PROTOCOL}://${SERVER}/
${JOBS URL}     ${PROTOCOL}://${SERVER}/sats/jobs
${EDIT JOB URL}     ${PROTOCOL}://${SERVER}/jobs/edit
${JOB CSS SELECTOR}     div.main div.wrapper div.main div.jobs-table-parent div.card div.react-bootstrap-table table.table tbody:nth-child(2) tr:nth-child(1) td.selection-cell:nth-child(1) > input.selection-input-4

*** Keywords ***
Open Browser To Login Page
    Log     Browser: ${BROWSER}                 INFO    console=True
    Log     Browser options: ${BROWSER OPTS}    INFO    console=True
    Log     Server: ${SERVER}                   INFO    console=True
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Set Selenium Speed    ${DELAY}
    Login Page Should Be Open

Login Page Should Be Open
    Location Should Be    ${LOGIN URL}

Login To Portal
    Go To    ${LOGIN URL}
    Input Text    username    ${VALID USER}
    Input Text    password    ${VALID PASSWORD}
    Click Button    sign-in-btn
    Wait Until Location Is      ${HOME URL}     2

Go To Jobs Page
    Login To Portal
    Go To   ${JOBS URL}
    Location Should Be  ${JOBS URL}

Go To Job Edit Page
    Go To Jobs Page
    Wait Until Element Is Visible   css:${JOB CSS SELECTOR}     2
    Click Element   css:${JOB CSS SELECTOR}
    Click Button    edit-btn
    Wait Until Location Is  ${EDIT JOB URL}     2

Dropdown Selection Correctly Shows and Hides Elements
    [Arguments]     ${label}    ${visible}  ${invisible1}   ${invisible2}
    Go To Job Edit Page
    Select From List By Label   schedDropdown   ${label}
    Element Should Be Visible   ${visible}
    Element Should Not Be Visible   ${invisible1}
    Element Should Not Be Visible   ${invisible2}

*** Test Cases ***
Edit Page Can Be Accessed
    Go To Job Edit Page

Cancel Button Works
    Go To Job Edit Page
    Click Button    cancel-btn
    Location Should Be  ${JOBS URL}

Selecting Daily In Dropdown Shows and Hides Correct Elements
    [Template]      Dropdown Selection Correctly Shows and Hides Elements
    Daily   dailyGroup      weeklyGroup     cronGroup
    Weekly  weeklyGroup     dailyGroup      cronGroup
    Something else  cronGroup   weeklyGroup     dailyGroup

Selecting Manual Execution In Dropdown Hides Correct Elements
    Go To Job Edit Page
    Select From List By Label   schedDropdown   Not scheduled (run manually)
    Element Should Not Be Visible   dailyGroup
    Element Should Not Be Visible   weeklyGroup
    Element Should Not Be Visible   cronGroup

Submission Fails If Cron String Is Invalid
    Go To Job Edit Page
    Select From List By Label   schedDropdown   Something else
    Input Text  cronField   invalid
    Click Button    submit-btn
    Wait Until Element Is Visible   invalidCronDialog   0.5
    Click Button    invalidCronDialogBtn
    Wait Until Location Is  ${EDIT JOB URL}     2
