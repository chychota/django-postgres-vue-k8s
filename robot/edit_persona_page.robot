*** Settings ***
Documentation     Tests persona table on Settings page.
Library           Collections
Library           String
Library           SeleniumLibrary
Library           RequestsLibrary
Library           PortalLibrary/BasicAuth.py
Suite Setup       Setup
Suite Teardown    Teardown

*** Variables ***
${SERVER}         localhost
${PROTOCOL}       https
${BROWSER}        Firefox
${DELAY}          0
${HOME URL}       ${PROTOCOL}://${SERVER}/
${HOME URL NO SLASH}    ${PROTOCOL}://${SERVER}
${LOGIN URL}      ${PROTOCOL}://${SERVER}/auth/sign-in
${SETTINGS URL}   ${PROTOCOL}://${SERVER}/settings
${EDIT PERSONA URL}     ${PROTOCOL}://${SERVER}/personas/edit
${USERNAME}       placeholder
${AUTH TOKEN}     placeholder
${VALID PASSWORD}   admin

*** Keywords ***
Setup
    Log     Browser: ${BROWSER}                 INFO    console=True
    Log     Browser options: ${BROWSER OPTS}    INFO    console=True
    Log     Server: ${SERVER}                   INFO    console=True
    Set Selenium Speed    ${DELAY}
    Create Dummy User
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Login To Portal
    Open Browser To Settings Page

Teardown
    Close Browser

Create Dummy User
    ${temp username}=    Generate Random String      8   [LETTERS][NUMBERS]
    Set Suite Variable     ${USERNAME}    ${temp username}
    Log     Dummy user will have ${USERNAME} as both their username and password    INFO    console=True
    Send Register Request   ${USERNAME}   ${USERNAME}

Send Register Request
    [Arguments]     ${username}     ${password}
    ${basic auth}=      Generate Basic Auth     admin       ${VALID PASSWORD}
    Log     ${basic auth}     INFO    console=True
    Create session      portal      ${HOME URL NO SLASH}
    &{data}=        Create dictionary   username=${username}  password=${password}  email=dummy@nokia.com
    ${headers}=     Create dictionary   Authorization=${basic auth}
    ${resp}=    Post request    portal      /api/auth/register/     json=${data}    headers=${headers}
    Log     ${resp}     INFO    console=True
    Log     ${resp.json()}     INFO    console=True
    Status Should Be    200     ${resp}
    Dictionary Should Contain Key   ${resp.json()}      token
    ${temp auth token}=      Get From Dictionary    ${resp.json()}   token
    Set Suite Variable      ${AUTH TOKEN}   ${temp auth token}
    Log     Auth token is now ${AUTH TOKEN}   INFO    console=True

Login To Portal
    Go To    ${LOGIN URL}
    Input Text    username    ${username}
    Input Text    password    ${username}
    Click Button    sign-in-btn
    Wait Until Location Is      ${HOME URL}     2

Open Browser To Settings Page
    # Go to homepage to force a refresh of setting page
    Go To   ${HOME URL}
    Go To   ${SETTINGS URL}

Check Retain Time Checkbox
    ${checked}=     Run Keyword And Return Status   Wait Until Element Is Visible      retain-time-dropdown     0.3
    Run Keyword Unless  ${checked}      Click Element   set-retain-time-checkbox

Set Persona Edit Page Values
    [Arguments]     ${persona_name}     ${retain_time_days}
    Wait Until Location Is      ${EDIT PERSONA URL}     2
    Wait Until Element Is Visible   name    2
    Input Text      name    ${persona_name}
    Check Retain Time Checkbox
    Wait Until Element Is Visible   retain-time-dropdown    2
    Input Text      retain_time     ${retain_time_days}

Submit Persona Edits
    [Arguments]     ${expected_dialog_msg}
    Click Button    submit-btn
    Wait Until Element Is Visible   dialog  2
    Element Text Should Be  dialog-msg    ${expected_dialog_msg}

Create Persona
    [Arguments]     ${create_btn}  ${persona_name}     ${retain_time_days}  ${expected_dialog_str}
    Open Browser To Settings Page
    Wait Until Element Is Visible   ${create_btn}   5
    Click Button    ${create_btn}
    Set Persona Edit Page Values    ${persona_name}     ${retain_time_days}
    Submit Persona Edits    ${expected_dialog_str}

Verify Personal Persona Matches Values
    [Arguments]     ${persona_name}     ${retain_time_str}  ${default_str}
    Open Browser To Settings Page
    Wait Until Element Is Visible   personal-personas-table     2
    Element Text Should Be  personal-persona|key=${persona_name}|name=name  ${persona_name}
    Element Text Should Be  personal-persona|key=${persona_name}|name=retain_time  ${retain_time_str}
    Element Text Should Be  personal-persona|key=${persona_name}|name=isDefault  ${default_str}

Open Persona For Editing
    [Arguments]     ${persona_table_html_id}     ${persona_name}
    Open Browser To Settings Page
    Wait Until Element Is Visible   ${persona_table_html_id}     2
    Click Button    personal-persona|key=${persona_name}|name=edit-btn
    Wait Until Location Is      ${EDIT PERSONA URL}     2

Open Personal Persona For Editing
    [Arguments]     ${persona_name}
    Open Persona For Editing    personal-personas-table     ${persona_name}

Set Personal Persona As Default Persona
    [Arguments]     ${persona_name}
    Open Browser To Settings Page
    Wait Until Element Is Visible   personal-personas-table     2
    Click Button    personal-persona|key=${persona_name}|name=set-default-persona-btn
    Wait Until Element Is Visible   confirm-dialog  2
    Click Button    confirm-dialog-yes-btn
    Wait Until Element Is Visible   message-dialog-msg  2
    Element Text Should Be  message-dialog-msg  Your default persona has been updated to "${persona_name}".

*** Test Cases ***
If User Has No Personas They Can Create Their First Persona
    Create Persona      create-first-persona-btn    persona1    2   Changes saved.
    Verify Personal Persona Matches Values  persona1    2 days  Default

Can Create Persona After First Persona
    Open Browser To Settings Page
    Wait Until Element Is Visible   personal-personas-table     2
    Create Persona      create-persona-btn    persona2    3     Changes saved.
    Verify Personal Persona Matches Values  persona2    3 days  ${EMPTY}

Cannot Create Two Personas With Same Name
    Open Browser To Settings Page
    Wait Until Element Is Visible   personal-personas-table     2
    Create Persona      create-persona-btn    persona1    1     A personal persona already exists with the name "persona1"

Can Edit Persona Name
    Open Personal Persona For Editing   persona1
    Set Persona Edit Page Values    persona1-edited     2
    Submit Persona Edits    Changes saved.
    Verify Personal Persona Matches Values      persona1-edited     2 days      Default

Can Edit Persona Retain Time
    Open Personal Persona For Editing   persona2
    Set Persona Edit Page Values    persona2     1
    Submit Persona Edits    Changes saved.
    Verify Personal Persona Matches Values      persona2     1 days      ${EMPTY}

Message Displays If User Checks Retain Time Checkbox But Does Not Specify Retain Time
    Open Personal Persona For Editing   persona2
    Set Persona Edit Page Values    persona2     ${EMPTY}
    Submit Persona Edits    If retain time checkbox is checked, retain time must be set. Either uncheck retain time checkbox, or set retain time.

Edit Page Should Display Name And Retain Time Properly
    Open Personal Persona For Editing   persona2
    Wait Until Element Is Visible   name    2
    Wait Until Element Is Visible   retain-time-dropdown    2
    Element Attribute Value Should Be   name    value   persona2
    Element Attribute Value Should Be   retain_time    value   1

Cannot Set Personal Persona To Same Name As Other Personal Persona
    Open Personal Persona For Editing   persona2
    Set Persona Edit Page Values    persona1-edited    2
    Submit Persona Edits    A personal persona already exists with the name "persona1-edited"
    # Confirm values and personal persona table are unchanged
    Open Browser To Settings Page
    Wait Until Element Is Visible   personal-personas-table     2
    Element Text Should Be    personal-persona|key=persona2|name=name    persona2

Can Change Default Persona
    Set Personal Persona As Default Persona     persona2
    # Verify personal personal table has persona1 not default, and persona2 as default
    Element Text Should Be    personal-persona|key=persona1-edited|name=isDefault    ${EMPTY}
    Element Text Should Be    personal-persona|key=persona2|name=isDefault    Default

Cancelling Setting Default Persona Does Not Affect Persona Values
    Open Browser To Settings Page
    Wait Until Element Is Visible   personal-personas-table     2
    Click Button    personal-persona|key=persona1-edited|name=set-default-persona-btn
    Wait Until Element Is Visible   confirm-dialog  2
    Click Button    confirm-dialog-no-btn
    # For whatever reason, we need to pause for a moment before checking the text
    Sleep    0.5
    Element Text Should Be    personal-persona|key=persona1-edited|name=isDefault    ${EMPTY}
    Element Text Should Be    personal-persona|key=persona2|name=isDefault    Default

Personas Can Be Deleted
    Open Browser To Settings Page
    Wait Until Element Is Visible   personal-personas-table     2
    Click Button    personal-persona|key=persona1-edited|name=delete-btn
    # Confirm dialog should appear
    Wait Until Element Is Visible   confirm-dialog  2
    Click Button    confirm-dialog-yes-btn
    Wait Until Element Is Visible   message-dialog  2
    Element Text Should Be    message-dialog-msg    Persona "persona1-edited" was deleted.
    # persona1-edited should not appear in table after it was deleted
    Element Should Not Be Visible    personal-persona|key=persona1-edited|name=name

Cannot Delete Persona If Persona Is Default
    Open Browser To Settings Page
    Wait Until Element Is Visible   personal-personas-table     2
    Element Text Should Be    personal-persona|key=persona2|name=isDefault    Default
    Click Button    personal-persona|key=persona2|name=delete-btn
    Wait Until Element Is Visible   message-dialog  2
    Element Text Should Be    message-dialog-msg    The persona you are trying to delete is your default persona. To delete this persona, set another persona as your default persona, and try again.
    Element Should Be Visible    personal-persona|key=persona2|name=name
    Element Text Should Be    personal-persona|key=persona2|name=isDefault    Default
