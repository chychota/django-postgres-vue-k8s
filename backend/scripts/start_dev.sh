#!/bin/bash
set -x

cd backend
rm -f ./*/migrations/*_initial.py ./*/migrations/*_auto_*.py
python3 manage.py collectstatic --no-input
python3 manage.py makemigrations --no-input
python3 manage.py migrate
python3 manage.py initadmin
python3 manage.py loaddata userprofile.json
python3 manage.py loaddata servers.json
python3 manage.py runserver 0.0.0.0:8000
