#!/bin/bash
set -x

python3 manage.py initadmin
python3 manage.py loaddata userprofile.json
python3 manage.py loaddata servers.json
gunicorn backend.wsgi -b 127.0.0.1:3000 --workers=2
