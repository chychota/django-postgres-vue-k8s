from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication
from servers import serializers, models
from knox.auth import TokenAuthentication



class ResultsViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    queryset = models.Results.objects.all().order_by('created_at')
    serializer_class = serializers.ResultsSerializer
    permission_classes = [IsAuthenticated]


class JenkinsViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, )
    queryset = models.Jenkins.objects.all()
    serializer_class = serializers.JenkinsSerializer
    permission_classes = [IsAuthenticated]


class JobViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, )
    queryset = models.Job.objects.all()
    serializer_class = serializers.JobSerializer
    permission_classes = [IsAuthenticated]


class RunJobViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, )
    queryset = models.RunJob.objects.all()
    serializer_class = serializers.RunJobSerializer
    permission_classes = [IsAuthenticated]


class ParameterViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, )
    queryset = models.Parameter.objects.all()
    serializer_class = serializers.ParameterSerializer
    permission_classes = [IsAuthenticated]


class JenkinsKeyViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, )
    queryset = models.JenkinsKey.objects.all()
    serializer_class = serializers.JenkinsKeySerializer
    permission_classes = [IsAuthenticated]


class TestlinkKeyViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, )
    serializer_class = serializers.TestlinkKeySerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
         return models.TestlinkKey.objects.filter(user=self.request.user)


class ElasticsearchViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, )
    queryset = models.Elasticsearch.objects.all()
    serializer_class = serializers.ElasticsearchSerializer
    permission_classes = [IsAuthenticated]
