import celery
import time
from backend.celery_app import app
from servers import models
from user_profile.models import UserProfile
import subprocess
import requests
from core.models import User

class BaseTask(celery.Task):
    pass

@app.task(bind=True, base=BaseTask)
def trigger_jobs(self, id):
    print("Doing async task")
    time.sleep(10)
    runjob = models.RunJob.objects.get(pk=id)
    job = models.Job.objects.get(pk=runjob.job.id)
    jenkins = models.Jenkins.objects.get(pk=job.jenkins.id)
    key = models.JenkinsKey.objects.get(pk=runjob.key.id)
    port = str(jenkins.port)
    parameters = models.Parameter.objects.filter(jobrun=runjob)

    if (jenkins.ssl):
        protocol = "https://"
    else:
        protocol = "http://"

    print("Downloading the client")
    url = protocol + jenkins.hostname + ":" + port + "/jnlpJars/jenkins-cli.jar"
    print(url)
    if (jenkins.certificatecheck):
        r = requests.get(url, stream=True)
    else:
        r = requests.get(url, stream=True, verify=False)

    with open('jenkins-cli.jar', 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            f.write(chunk)
    userprofile = UserProfile.objects.get(user=runjob.issuer)
    user = User.objects.get(pk=runjob.issuer.id)
    username = key.username
    apikey = key.key
    jenkins_secret = username + ':' + apikey
    ci_jenkins_url = protocol + jenkins.hostname + ":" + port

    if (jenkins.certificatecheck):
        cmd = ['java', '-jar', 'jenkins-cli.jar', '-s', ci_jenkins_url,
        '-auth', jenkins_secret, 'build', job.name, '-s', '-v']
    else:
        cmd = ['java', '-jar', 'jenkins-cli.jar', '-noCertificateCheck', '-s', ci_jenkins_url,
        '-auth', jenkins_secret, 'build', job.name, '-s', '-v']

    for x in parameters:
        cmd += ['-p']
        parm = x.name + "=" + x.value
        cmd += [ parm ]

    print(cmd)
    subprocess.call(cmd)
    print("Task is done")
    return
