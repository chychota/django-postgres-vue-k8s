from rest_framework import serializers
from servers import models
from django.contrib.auth import get_user_model


class ResultsUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'username', 'first_name', 'last_name', 'email')
        ref_name = "Results User"


class ResultsSerializer(serializers.ModelSerializer):
    owner = ResultsUserSerializer(read_only=True)

    class Meta:
        model = models.Results
        fields = ('id', 'name', 'hostname', 'port', 'username',
                  'password', 'path', 'owner')


class JenkinsSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Jenkins
        fields = ('id', 'name', 'hostname', 'port', 'ssl', 'certificatecheck')


class TestlinkSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Testlink
        fields = ('id', 'name', 'hostname', 'port', 'ssl', 'certificatecheck')


class JobSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Job
        fields = ('id', 'name', 'jenkins')

    def to_representation(self, instance):
        self.fields['jenkins'] = JenkinsSerializer(read_only=True)
        return super(JobSerializer, self).to_representation(instance)


class RunJobSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.RunJob
        fields = ('id', 'job', 'key', 'issuer')


class JenkinsKeySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.JenkinsKey
        fields = ('id', 'name','username','key')


class TestlinkKeySerializer(serializers.ModelSerializer):
    testlink = TestlinkSerializer(read_only=True)

    class Meta:
        model = models.TestlinkKey
        fields = ('id', 'name', 'key', 'user', 'testlink')

    def to_representation(self, instance):
        self.fields['testlink'] = TestlinkSerializer(read_only=True)
        return super(TestlinkKeySerializer, self).to_representation(instance)


class ElasticsearchSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Elasticsearch
        fields = ('id', 'name', 'hostname', 'port', 'ssl', 'certificatecheck')


class ParameterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Parameter
        fields = ('id', 'name', 'value', 'jobrun')
