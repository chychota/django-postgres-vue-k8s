from django.test import TestCase
from django.contrib.auth.models import User
from .models import Jenkins


class JenkinsModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Create a user
        testuser1 = User.objects.create_user(
            username='testuser1', password='abc123')
        testuser1.save()

        # Create a jenkins server
        test_jenkins = Jenkins.objects.create(
            id='9d68b70b-e515-4aab-b48d-18e512c8c855', name='Test Jenkins', hostname='test.jenkins.local',
            port=443, owner=testuser1, protected=True)
        test_jenkins.save()

    def test_jenkins_content(self):
        jenkins = Jenkins.objects.get(pk='9d68b70b-e515-4aab-b48d-18e512c8c855')
        name = f'{jenkins.name}'
        hostname = f'{jenkins.hostname}'
        port = f'{jenkins.port}'
        owner = f'{jenkins.owner}'
        protected = f'{jenkins.protected}'
        self.assertEqual(name, 'Test Jenkins')
        self.assertEqual(hostname, 'test.jenkins.local')
        self.assertEqual(port, '443')
        self.assertEqual(owner, 'testuser1')
        self.assertEqual(protected, 'True')
