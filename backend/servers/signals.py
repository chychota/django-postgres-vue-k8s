from django.db.models.signals import post_save
from django.dispatch import receiver
from servers.models import *
from servers.tasks import trigger_jobs

@receiver(post_save, sender=RunJob)
def trigger_job(sender, instance, created, **kwargs):
    if created:
        trigger_jobs.delay(instance.id)
