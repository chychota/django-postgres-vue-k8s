from servers import views
from django.urls import path, include
from rest_framework import routers

router = routers.SimpleRouter()
router.register('jenkins', views.JenkinsViewSet)
router.register('results', views.ResultsViewSet)
router.register('elasticsearch', views.ElasticsearchViewSet)
router.register('jobs', views.JobViewSet, 'jobs')
router.register('runjob', views.RunJobViewSet)
router.register('parameter', views.ParameterViewSet)
router.register('jenkinskeys', views.JenkinsKeyViewSet, 'jenkinskeys')
router.register('testlinkkeys', views.TestlinkKeyViewSet, 'testlinkkeys')

urlpatterns = [
    path('', include(router.urls)),
]
