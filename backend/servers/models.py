from django.db import models
from django.conf import settings
import uuid


class Results(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100, unique=True)
    hostname = models.CharField(max_length=100)
    port = models.IntegerField(null=False, default=22)
    username = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    path = models.CharField(max_length=1000)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.hostname


class Jenkins(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100, unique=True)
    hostname = models.CharField(max_length=100)
    port = models.IntegerField(null=False, default=80)
    ssl = models.BooleanField(null=False, default=False)
    certificatecheck = models.BooleanField(null=True, default=True)

    def __str__(self):
        return self.name


class Testlink(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100, unique=True)
    hostname = models.CharField(max_length=100)
    port = models.IntegerField(null=False, default=80)
    ssl = models.BooleanField(null=False, default=False)
    certificatecheck = models.BooleanField(null=True, default=True)

    def __str__(self):
        return self.name


class Job(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    jenkins = models.ForeignKey(Jenkins, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)

    def __str__(self):
        return '%s: %s' % (self.jenkins, self.name)


class JenkinsKey(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    key = models.CharField(max_length=34, default='', blank=True)
    username = models.CharField(max_length=100)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class TestlinkKey(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    key = models.CharField(max_length=32, default='', blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    testlink = models.ForeignKey(Testlink, on_delete=models.CASCADE)

    class Meta:
        ordering = ['user', 'testlink']

    def __str__(self):
        return '%s: %s: %s' % (self.user, self.testlink, self.name)


class RunJob(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    key = models.ForeignKey(JenkinsKey, on_delete=models.SET_NULL, null=True, )
    issuer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return '%s: %s' % (self.issuer, self.job)

class Parameter(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    value = models.CharField(max_length=100)
    jobrun = models.ForeignKey(RunJob, on_delete=models.CASCADE)

    def __str__(self):
        return '%s: %s: %s' % (self.jobrun.job, self.name, self.value)


class Elasticsearch(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100, unique=True)
    hostname = models.CharField(max_length=100)
    port = models.IntegerField(null=False, default=9200)
    ssl = models.BooleanField(null=False, default=False)
    certificatecheck = models.BooleanField(null=True, default=True)

    def __str__(self):
        return self.name






