from django.contrib import admin
from servers import models


class IdAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

admin.site.register(models.Elasticsearch, IdAdmin)
admin.site.register(models.Results, IdAdmin)
admin.site.register(models.Jenkins, IdAdmin)
admin.site.register(models.Job, IdAdmin)
admin.site.register(models.RunJob, IdAdmin)
admin.site.register(models.Parameter, IdAdmin)
admin.site.register(models.Testlink, IdAdmin)
admin.site.register(models.JenkinsKey, IdAdmin)
admin.site.register(models.TestlinkKey, IdAdmin)
