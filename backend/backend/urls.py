from django.contrib import admin
from django.urls import path, include
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
import os


schema_view = get_schema_view(
   openapi.Info(
      title="NTA API",
      default_version='v1',

      description="NTA Portal API",
      contact=openapi.Contact(email="james.chychota.ext@nokia.com"),
   ),
   url=os.environ.get("SWAGGER_URL", "http://localhost/"),
   public=True,
   permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
    path('accounts/', include('accounts.urls')),
    path('profile/', include('user_profile.urls')),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0)),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0)),
    path('servers/', include('servers.urls')),
    path('api/', include('core.urls')),
]
