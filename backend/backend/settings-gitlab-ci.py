from .settings import * # noqa

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'backend',
        'USER': 'runner',
        'PASSWORD': '',
        'HOST': 'postgres',
        'PORT': '5432',
    },
}
