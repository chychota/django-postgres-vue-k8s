from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from .models import UserProfile
from .serializers import UserProfileSerializer
from knox.auth import TokenAuthentication
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated


class GetUserProfileView(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    def get(self, request, format=None):
        try:
            user = self.request.user
            username = user.username

            user_profile = UserProfile.objects.get(user=user)
            user_profile = UserProfileSerializer(user_profile)

            return Response({ 'profile': user_profile.data, 'username': str(username) })
        except:
            return Response({ 'error': 'Something went wrong when retrieving profile' })


class UpdateUserProfileView(APIView):
    authentication_classes = (SessionAuthentication, )
    def put(self, request, format=None):
        try:
            user = self.request.user
            username = user.username

            data = self.request.data
            first_name = data['first_name']
            last_name = data['last_name']
            email = data['email']
            phone = data['phone']
            province = data['province']
            city = data['city']
            country = data['country']

            UserProfile.objects.filter(user=user).update(first_name=first_name, last_name=last_name,
                email=email, phone=phone, city=city, province=province, country=country)

            user_profile = UserProfile.objects.get(user=user)
            user_profile = UserProfileSerializer(user_profile)

            return Response({ 'profile': user_profile.data, 'username': str(username) })
        except:
            return Response({ 'error': 'Something went wrong when updating profile' })


class UpdateTokenView(APIView):
    authentication_classes = (SessionAuthentication, )
    def put(self, request, format=None):
        try:
            user = self.request.user
            username = user.username

            data = self.request.data
            portal_knox_token = data['portal_knox_token']

            UserProfile.objects.filter(user=user).update(portal_knox_token=portal_knox_token)

            user_profile = UserProfile.objects.get(user=user)
            user_profile = UserProfileSerializer(user_profile)

            return Response({ 'profile': user_profile.data, 'username': str(username) })
        except:
            return Response({ 'error': 'Something went wrong when updating profile' })
