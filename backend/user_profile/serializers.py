from rest_framework import serializers
from .models import UserProfile
from servers.models import Jenkins, Testlink

class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = '__all__'

class TokenProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = ('portal_knox_token', )
