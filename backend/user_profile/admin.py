from django.contrib import admin
from user_profile import models

class IdAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

admin.site.register(models.UserProfile, IdAdmin)
