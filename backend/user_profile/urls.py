from django.urls import path
from rest_framework import routers
from user_profile import views

urlpatterns = [
    path('user', views.GetUserProfileView.as_view()),
    path('update', views.UpdateUserProfileView.as_view()),
    path('token', views.UpdateTokenView.as_view()),
]
