from django.db import models
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from servers.models import Jenkins, Testlink
import uuid

class UserProfile(models.Model):

    class Role(models.TextChoices):
            GUEST = 'GU', _('Guest')
            REPORTER = 'RE', _('Reporter')
            DEVELOPER = 'DE', _('Developer')
            MAINTAINER = 'ME', _('Maintainer')
            OWNER = 'OW', _('Owner')

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255, default='', blank=True)
    last_name = models.CharField(max_length=255, default='', blank=True)
    email = models.EmailField(max_length=254, default='', blank=True)
    phone = models.CharField(max_length=20, default='', blank=True)
    city = models.CharField(max_length=20, default='', blank=True)
    province = models.CharField(max_length=20, default='', blank=True)
    country = models.CharField(max_length=20, default='', blank=True)
    portal_knox_token = models.CharField(max_length=64, default='', blank=True)
    isAdmin = models.BooleanField(default=False)
    sats_role = models.CharField(max_length=2,choices=Role.choices,default=Role.GUEST)
    cats_role = models.CharField(max_length=2,choices=Role.choices,default=Role.GUEST)
    pats_role = models.CharField(max_length=2,choices=Role.choices,default=Role.GUEST)
    oats_role = models.CharField(max_length=2,choices=Role.choices,default=Role.GUEST)
    dats_role = models.CharField(max_length=2,choices=Role.choices,default=Role.GUEST)
    wfm_role = models.CharField(max_length=2,choices=Role.choices,default=Role.GUEST)
    devops_role = models.CharField(max_length=2,choices=Role.choices,default=Role.GUEST)

    def __str__(self):
        return self.user.username
