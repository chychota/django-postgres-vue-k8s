from django.urls import path, re_path
from core import views

urlpatterns = [
    path('v1/redis/stream/', views.RedisAPI.as_view()),
    re_path(r'v1/poll_async_results/(?P<task_id>[0-9A-Za-z_\-]+)/?$', views.PollAsyncResultsView.as_view()),
    re_path(r'v1/elasticsearch/query/(?P<es_id>[0-9A-Za-z_\-]+)/?$', views.QueryESView.as_view()),
]