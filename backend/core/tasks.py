import celery
from backend.celery_app import app
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q
from .models import AsyncResults
import json

class BaseTask(celery.Task):
    pass

@app.task(bind=True, base=BaseTask)
def esearch(self, user, server, **kwargs):
    field = "user_info.user.username"
    task_id = self.request.id
    try:
        client = Elasticsearch([server])
        q = Q("bool", should=[ Q('match', ** { field: user}) & Q('match', robot_type='suite')], minimum_should_match=1)
        s = Search(using=client, index="xats_*").query(q)
        response = s.execute()
        print('Total %d hits found.' % response.hits.total.value)
        result = {
            "status_code": 200,
            "hits": response.hits.total.value
        }
        json_result = json.dumps(result)
        AsyncResults.objects.create(task_id=task_id,result=json_result)
        return json_result

    except:
        result = {"status_code": 500,
                  "error_message": str(sys.exc_info()[0])}
        json_result = json.dumps(result)
        AsyncResults.objects.create(task_id=task_id, result=json_result)
        return json_result