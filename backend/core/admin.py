from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, AsyncResults

class IdAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)

admin.site.register(User, UserAdmin)
admin.site.register(AsyncResults, IdAdmin )