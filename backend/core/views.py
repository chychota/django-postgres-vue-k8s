import os
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from walrus import Database
from knox.auth import TokenAuthentication
from rest_framework.authentication import SessionAuthentication
from core.tasks import esearch
from .models import AsyncResults
from servers.models import Elasticsearch
import json


class RedisAPI(generics.GenericAPIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)

    hostname = os.environ.get('REDIS_SERVICE_HOST', 'redis')
    port = 6379
    redis_pass = os.environ.get('REDIS_PASS', None)
    db = Database(host=hostname, port=port, password=redis_pass)

    def post(self, request, *args, **kwargs):
        data = request.data

        if not isinstance(data, dict):
            resp = {
                'message': 'Error: request body is not a valid JSON object'
            }
            return Response(resp, status=status.HTTP_400_BAD_REQUEST)

        if not (data.keys() >= {'stream', 'message'}):
            resp = {
                'message': 'Error: request body must contain "stream" and "message" keys'
            }
            return Response(resp, status=status.HTTP_400_BAD_REQUEST)

        if (not data['message']) or (not isinstance(data['message'], dict)):
            resp = {
                'message': 'Error: "message" must be a non-empty JSON object'
            }
            return Response(resp, status=status.HTTP_400_BAD_REQUEST)

        stream = RedisAPI.db.Stream(data['stream'])
        stream.add(data['message'])

        resp = {
            'message': 'Pushed message to redis stream'
        }
        return Response(resp, status=status.HTTP_200_OK)


class QueryESView(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    
    def get(self, request, *args, **kwargs):
        es_id = self.kwargs.get("es_id", None)
        es_server = Elasticsearch.objects.get(pk=es_id)
        port = str(es_server.port)
        if (es_server.ssl):
            protocol = "https://"
        else:
            protocol = "http://"
        url = protocol + es_server.hostname + ":" + port
        user = request.user.username
        task = esearch.delay(user, url)
        response = {"task_id": task.task_id}
        return Response(response, status=status.HTTP_202_ACCEPTED)


class PollAsyncResultsView(APIView):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    
    def get(self, request, *args, **kwargs):
        task_id = self.kwargs.get("task_id", None)
        async_result = AsyncResults.objects.get(task_id=task_id)
        if async_result:
            load_body = json.loads(async_result.result)
            status_code = load_body.get("status_code", None)
            if status_code == 500:
                return Response(
                    status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            else:
                return Response(status=status.HTTP_200_OK, data=load_body)
        else:
            return Response(status=status.HTTP_202_ACCEPTED)