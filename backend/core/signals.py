from django.dispatch import receiver
from django_auth_ldap.backend import populate_user, LDAPBackend
from user_profile.models import UserProfile


@receiver(populate_user, sender=LDAPBackend)
def ldap_auth_handler(user, ldap_user, **kwargs):
    user_list = user.first_name.split(' ')
    user.last_name = user_list[0]
    user.first_name = user_list[1]
    user.save()

    try:
        UserProfile.objects.get_or_create(user=user, first_name=user_list[1], last_name=user_list[0],
            email=user.email, phone='', city='', portal_knox_token='')
    except:
        print("Profile already exists")
