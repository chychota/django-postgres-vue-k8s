from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model


class Command(BaseCommand):

    def handle(self, *args, **options):
        User = get_user_model()
        if User.objects.count() == 0:
            id = '04f541ed-5695-4445-9fdb-be0b11d625c4'
            username = settings.ADMIN_USER.replace(' ', '')
            email = settings.ADMIN_EMAIL
            password = settings.ADMIN_PASSWORD
            print('Creating account for %s (%s)' % (username, email))
            admin = User.objects.create_superuser(
                id=id,
                email=email,
                username=username,
                password=password
            )
            admin.is_active = True
            admin.is_admin = True
            admin.save()
        else:
            print(
                'Admin accounts can only be initialized if no Accounts exist'
            )
