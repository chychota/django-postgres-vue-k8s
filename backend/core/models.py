import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext as _


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

class AsyncResults(models.Model):
  id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
  task_id = models.CharField(blank=False, max_length=255, null=False, verbose_name=_("task id"), db_index=True)
  result = models.TextField(blank=False, verbose_name=_("task result"))
  created_on = models.DateTimeField(auto_now_add=True)
