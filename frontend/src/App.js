import React from "react";
import { Provider } from "react-redux";
import ReduxToastr from "react-redux-toastr";

import store from "./redux/store/index";
import Routes from "./routes/Routes";

const App = () => (
  <Provider store={store}>
    <Routes />
    <ReduxToastr
      timeOut={5000}
      newestOnTop={true}
      preventDuplicates
      position="top-center"
      transitionIn="bounceInDown"
      transitionOut="bounceOutUp"
      progressBar
      closeOnToastrClick
    />
  </Provider>
);

export default App;
