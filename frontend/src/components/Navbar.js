import React from "react";
import { connect } from "react-redux";
import { logout } from "../redux/actions/authActions";
import { toggleSidebar } from "../redux/actions/sidebarActions";
import { Link } from "react-router-dom";

import {
  Button,
  Collapse,
  Navbar,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

import {
  Settings,
  User,
} from "react-feather";

import avatar1 from "../assets/img/avatars/avatar.png";

const NavbarComponent = ({ isAuthenticated, username, logout, toggleSidebar }) => {

  const authLinks = (
    <React.Fragment>
      <UncontrolledDropdown nav inNavbar>
          <span className="d-inline-block d-sm-none">
            <DropdownToggle nav caret>
              <Settings size={18} className="align-middle"/>
            </DropdownToggle>
          </span>
        <span className="d-none d-sm-inline-block">
            <DropdownToggle nav caret>
              <img
                src={avatar1}
                className="avatar img-fluid mr-1"
                alt="avatar"
              />
              <span className="text-dark">{username}</span>
            </DropdownToggle>
          </span>
        <DropdownMenu right>
          <Link to="/user/profile">
            <DropdownItem>
              <User size={18} className="align-middle mr-2"/>
              Profile
            </DropdownItem>
          </Link>
          <DropdownItem divider/>
          <Link to='#!' onClick={logout}>
            <DropdownItem>
              Sign out
            </DropdownItem>
          </Link>
        </DropdownMenu>
      </UncontrolledDropdown>
    </React.Fragment>
  );

  const guestLinks = (
    <React.Fragment>
      <li className='nav-item'>
        <Button href='/auth/sign-in' color='primary'>Sign In</Button>
      </li>
    </React.Fragment>
  );

  return (
    <Navbar color="white" light expand>
      <span
        className="sidebar-toggle d-flex mr-2"
        onClick={toggleSidebar}
      >
        <i className="hamburger align-self-center" />
      </span>
      <Collapse navbar>
        <Nav className="ml-auto" navbar>
          { isAuthenticated ? authLinks : guestLinks }
        </Nav>
      </Collapse>
    </Navbar>
  );
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  username: state.profile.username
});

export default connect(mapStateToProps, { logout, toggleSidebar })(NavbarComponent);

