import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { update_profile, login_knox } from "../redux/actions/profileActions";
import { delete_account } from "../redux/actions/authActions";
import { CopyToClipboard } from "react-copy-to-clipboard";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row,
  Col
} from "reactstrap";

const ProfileDetails = ({first_name_global, last_name_global, email_global, phone_global, city_global,
  country_global, portal_knox_token_global,
  update_profile, delete_account,  login_knox }) => {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    city: '',
    country: ''
  });

  const [isOpen, setOpen] = useState(false);

  const toggle = React.useCallback(() => {
    setOpen(value => !value);
  }, []);


  const {
    first_name,
    last_name,
    email,
    phone,
    city,
    country
  } = formData;

  useEffect(() => {
    setFormData({
      first_name: first_name_global,
      last_name: last_name_global,
      email: email_global,
      phone: phone_global,
      city: city_global,
      country: country_global
    });
    if (!portal_knox_token_global){login_knox()};
    }, [first_name_global, last_name_global, email_global, phone_global, city_global,
           country_global]);

  const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();

    update_profile(first_name, last_name, email, phone, city, country,
        testlink_api_key, jenkins_api_key, portal_knox_token_global);
  };

  return (
    <React.Fragment>
      <Card>
        <CardHeader>
          <CardTitle tag="h5">Welcome to your User Profile</CardTitle>
          <h6 className="card-subtitle text-muted">Update your user profile below:</h6>
        </CardHeader>
        <CardBody>
          <Form onSubmit={e => onSubmit(e)}>
            <FormGroup>
              <Label htmlFor='first_name'>First Name</Label>
              <Input
                type="text"
                name="first_name"
                placeholder={`${first_name_global}`}
                onChange={e => onChange(e)}
                value={first_name}
              />
            </FormGroup>
            <FormGroup>
              <Label>Last Name</Label>
              <Input
                type="text"
                name="last_name"
                placeholder={`${last_name_global}`}
                onChange={e => onChange(e)}
                value={last_name}
              />
            </FormGroup>
            <FormGroup>
              <Label>Email</Label>
              <Input
                type="text"
                name="email"
                placeholder={`${email_global}`}
                onChange={e => onChange(e)}
                value={email}
              />
            </FormGroup>
            <FormGroup>
              <Label>Phone</Label>
              <Input
                type="text"
                name="phone"
                placeholder={`${phone_global}`}
                onChange={e => onChange(e)}
                value={phone}
              />
            </FormGroup>
            <FormGroup>
              <Label>City</Label>
              <Input
                type="text"
                name="city"
                placeholder={`${city_global}`}
                onChange={e => onChange(e)}
                value={city}
              />
            </FormGroup>
            <FormGroup>
              <Label>Country</Label>
              <Input
                type="text"
                name="country"
                placeholder={`${country_global}`}
                onChange={e => onChange(e)}
                value={country}
              />
            </FormGroup>
            <FormGroup>
              <Label>TestLink API Key</Label>
              <Input
                type="text"
                name="testlink_api_key"
                placeholder={`${testlink_api_key_global}`}
                onChange={e => onChange(e)}
                value={testlink_api_key}
              />
            </FormGroup>
            <FormGroup>
              <Label>Jenkins API Key</Label>
              <Input
                type="text"
                name="jenkins_api_key"
                placeholder={`${jenkins_api_key_global}`}
                onChange={e => onChange(e)}
                value={jenkins_api_key}
              />
            </FormGroup>
            <Label>Auth Token</Label>
            <Row>
              <Col md={8}>
                <FormGroup>
                    <Input
                      type="text"
                      name="portal_knox_token"
                      placeholder={`${portal_knox_token_global}`}
                      value={portal_knox_token_global}
                    />
                </FormGroup>
               </Col>
                <Col md={4}>
                <CopyToClipboard text={portal_knox_token_global}>
                  <Button color="info">Copy to Clipboard</Button>
                </CopyToClipboard>
              </Col>
            </Row>
            <Button color="primary" type='submit'>Update Profile</Button>
          </Form>
          <p className='mt-3'>
            Click the button below to delete your user account:
          </p>
          <Button
            color="danger"
            href='#1'
            onClick={ toggle }
          >Delete</Button>
        </CardBody>
      </Card>
      <Modal isOpen={isOpen} centered>
        <ModalHeader>Delete Account</ModalHeader>
        <ModalBody className="text-center m-3">
          <p className="mb-0">
            Are you sure you want to delete your Account?
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={ toggle }>Cancel</Button>{" "}
          <Button color="danger" onClick={ delete_account }>
            Delete
          </Button>
        </ModalFooter>
      </Modal>
    </React.Fragment>
  )
};

const mapStateToProps = state => ({
  username: state.profile.username,
  first_name_global: state.profile.first_name,
  last_name_global: state.profile.last_name,
  email_global: state.profile.email,
  phone_global: state.profile.phone,
  city_global: state.profile.city,
  country_global: state.profile.country,
  portal_knox_token_global: state.profile.portal_knox_token,
});

export default connect(mapStateToProps, {
  update_profile,
  delete_account,
  login_knox,
})(ProfileDetails);
