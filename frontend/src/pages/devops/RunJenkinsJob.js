import React, { useState, useEffect } from "react";
import Select from "react-select";
import { connect } from "react-redux";
import { run_job } from "../../redux/actions/serverActions";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Container,
  Row,
  FormGroup,
  Label,
  Button,
  Form
} from "reactstrap";

const Runsats = ({ jenkins_keys_global, jobs_global, issuer, run_job }) => {

  const [jenkinsData, setjenkinsData] = useState({
    keys: jenkins_keys_global.map((jenkinskeys) => ({ value: jenkinskeys.id, label: jenkinskeys.name })),
    jobs: jobs_global.map((jobs) => ({ value: jobs.id, label: jobs.name + " on " + jobs.jenkins.name  })),
  });

  const [selectedOptions, setSelectedOptions] = useState({
    job: null,
    key: null,
  });



  useEffect(() => {
    setjenkinsData({
      keys: jenkins_keys_global.map((jenkinskeys) => ({ value: jenkinskeys.id, label: jenkinskeys.name })),
      jobs: jobs_global.map((jobs) => ({ value: jobs.id, label: jobs.name + " on " + jobs.jenkins.name })),
    });
  }, [ jenkins_keys_global, jobs_global]);

  const onJobChange = e => {
    setSelectedOptions({
      job: e.value,
      key: selectedOptions.key,
    });
  };

  const onKeyChange = e => {
    setSelectedOptions({
      job: selectedOptions.job,
      key: e.value
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    run_job(selectedOptions.job, selectedOptions.key, issuer);
  }

  return(
    <Container fluid className="p-0">
      <h1 className="h3 mb-3">Run Test</h1>
      <Row>
        <Col>
          <Card>
            <CardHeader>
              <CardTitle tag="h5" className="mb-0">Temp Development Selection</CardTitle>
            </CardHeader>
            <CardBody>
              <Form id="form" onSubmit={e => onSubmit(e)}>
                <Row>
                  <Col lg="4">
                    <FormGroup>
                      <Label>Select Test Case</Label>
                      <Select
                        className="react-select-container"
                        classNamePrefix="react-select"
                        options={jenkinsData.jobs}
                        onChange={e => onJobChange(e)}
                      />
                    </FormGroup>
                  </Col>
                  <Col lg="4">
                    <FormGroup>
                      <Label>Select Key</Label>
                      <Select
                        className="react-select-container"
                        classNamePrefix="react-select"
                        options={jenkinsData.keys}
                        onChange={e => onKeyChange(e)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Button color="primary" type='submit'>Run</Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  )
};

const mapStateToProps = state => ({
  jenkins_keys_global: state.keys.jenkinskeys,
  jobs_global: state.servers.jobs,
  issuer: state.profile.user,
});

export default connect(mapStateToProps, { run_job })(Runsats);
