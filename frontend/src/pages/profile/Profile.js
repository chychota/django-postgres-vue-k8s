import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import avatar from "../../assets/img/avatars/avatar.png";
import { login_knox } from "../../redux/actions/profileActions";
import { CopyToClipboard } from "react-copy-to-clipboard";

import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Container,
  Col,
  Row,
  Table,
} from "reactstrap";

import {
  User,
  Home,
  Phone,
  Mail,
  Copy,
} from "react-feather";


const ProfileDetails = ({ history, profile }) => {

  const noUsername = (
    <React.Fragment>
      {profile.username}
    </React.Fragment>
  );

  const hasUsername = (
    <React.Fragment>
      {profile.first_name}{" "}{profile.last_name}
    </React.Fragment>
  );
  return(
  <Card>
    <CardHeader>
      <CardTitle tag="h5" className="mb-0">
        Profile Details
      </CardTitle>
    </CardHeader>
    <CardBody className="text-center">
      <img
        src={avatar}
        className="img-fluid mb-2"
        alt="avatar"
        width="128"
        height="128"
        />
      <CardTitle tag="h5" className="mb-0">
        { profile.first_name ? hasUsername : noUsername}
      </CardTitle>
    </CardBody>

    <hr className="my-0" />
    <CardBody>
      <CardTitle tag="h5">About</CardTitle>
      <ul className="list-unstyled mb-0">
        <li className="mb-1">
          <User width={14} height={14} className="mr-1" /> Username:{" "}
          {profile.username}
        </li>

        <li className="mb-1">
          <Mail width={14} height={14} className="mr-1" /> Email:{" "}
          <a href={'mailto:${profile:email'}>{profile.email}</a>
        </li>

        <li className="mb-1">
          <Home width={14} height={14} className="mr-1" /> Location:{" "}
          {profile.city}{" "}{profile.province}{", "}{profile.country}
        </li>

        <li className="mb-1">
          <Phone width={14} height={14} className="mr-1" /> Phone:{" "}
          {profile.phone}
        </li>
      </ul>
    </CardBody>
    <hr className="my-0" />
    <CardBody>
      <Button color="primary" onClick={() => history.push('/user/settings')}>Edit Profile</Button>
    </CardBody>
  </Card>
  )
};

const Tokens = ({ portal_knox_token_global, login_knox }) => {
  const [formData, setFormData] = useState( "");

  useEffect(() => {
    setFormData({portal_knox_token_global});
  }, [portal_knox_token_global]);

  if (formData=== '')
    login_knox();

  return(
    <Card>
      <CardHeader>
        <CardTitle tag="h5" className="mb-0">
          API Authentication Token
        </CardTitle>
      </CardHeader>
      <Table striped>
        <thead>
          <tr>
            <th style={{ width: "30% "}}>Name</th>
            <th style={{ width: "50% "}}>Value</th>
            <th style={{ width: "20% "}}>Actions</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Authentication Token</td>
            <td>{portal_knox_token_global}</td>
            <td>
              <CopyToClipboard text={portal_knox_token_global}>
                <Copy/>
              </CopyToClipboard>
            </td>
          </tr>
        </tbody>
      </Table>
    </Card>
  )
};


const Key = ({name, username, apikey}) => (
  <tr>
    <td>{name}</td>
    <td>{username}</td>
    <td>{apikey}</td>
    <td>
      <CopyToClipboard text={apikey}>
        <Copy/>
      </CopyToClipboard>
    </td>
  </tr>
);

const TestlinkKeys = ({ testlink_keys_global }) => {
  const [testlinkkeysData, settestlinkkeysData] = useState({
    testlink_keys: testlink_keys_global,
  });

  useEffect(() => {
    settestlinkkeysData({
      testlink_keys: testlink_keys_global,
    });
  }, [testlink_keys_global]);

  return(
    <Card>
      <CardHeader>
        <CardTitle tag="h5" className="mb-0">
          Testlink Keys
        </CardTitle>
      </CardHeader>
      <Table striped>
        <thead>
        <tr>
          <th style={{ width: "20% "}}>Key Name</th>
          <th style={{ width: "20% "}}>Testlink Server</th>
          <th style={{ width: "40% "}}>Key</th>
          <th style={{ width: "20% "}}>Actions</th>
        </tr>
        </thead>
        <tbody>
        {testlinkkeysData.testlink_keys.map(({name, testlink, key}, index) => (
          <Key name={name} server={testlink.name} apikey={key} key={index}/>
        ))}
        </tbody>
      </Table>
    </Card>
  )
};

const Authorization = ({profile}) => {
  const getRole = (role) => {
    switch(role) {
      case 'GU': return 'Guest';
      case 'RE': return 'Reporter';
      case 'DE': return 'Developer';
      case 'ME': return 'Maintainer';
      case 'OW': return 'Owner';
      default: return 'Guest';
    }
  };

  return(
    <Card>
      <CardHeader>
        <CardTitle tag="h5" className="mb-0">
          Authorization Levels
        </CardTitle>
      </CardHeader>
      <Table striped>
        <thead>
        <tr>
          <th style={{ width: "50% "}}>Name</th>
          <th style={{ width: "50% "}}>Level</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>CATS</td>
          <td>{getRole(profile.cats_role)}</td>
        </tr>
        <tr>
          <td>DATS</td>
          <td>{getRole(profile.dats_role)}</td>
        </tr>
        <tr>
          <td>OATS</td>
          <td>{getRole(profile.oats_role)}</td>
        </tr>
        <tr>
          <td>PATS</td>
          <td>{getRole(profile.pats_role)}</td>
        </tr>
        <tr>
          <td>SATS</td>
          <td>{getRole(profile.sats_role)}</td>
        </tr>
        <tr>
          <td>WFM</td>
          <td>{getRole(profile.wfm_role)}</td>
        </tr>
        <tr>
          <td>DevOps</td>
          <td>{getRole(profile.devops_role)}</td>
        </tr>
        </tbody>
      </Table>
    </Card>
  )
};

const Profile = (props) => (
  <Container fluid className="p-0">
    <h1 className="h3 mb-3">Profile</h1>

    <Row>
      <Col md="4" xl="3">
        <ProfileDetails {...props}/>
      </Col>
      <Col md="8" xl="9">
        <Tokens {...props}/>
        <TestlinkKeys {...props}/>
        <Authorization {...props}/>
      </Col>
    </Row>
  </Container>
);

const mapStateToProps = state => ({
  profile: state.profile,
  portal_knox_token_global: state.profile.portal_knox_token,
  testlink_keys_global: state.keys.testlinkkeys,
});

export default withRouter(connect(mapStateToProps, { login_knox })(Profile));
