import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { update_profile } from "../../redux/actions/profileActions";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Col,
  Container
} from "reactstrap";


const EditProfile = ({first_name_global, last_name_global, email_global, phone_global,
                       city_global, province_global, country_global, update_profile,
                       delete_account, history}) => {
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    city: '',
    province: '',
    country: ''
  });


  const {
    first_name,
    last_name,
    email,
    phone,
    city,
    province,
    country
  } = formData;

  useEffect(() => {
    setFormData({
      first_name: first_name_global,
      last_name: last_name_global,
      email: email_global,
      phone: phone_global,
      city: city_global,
      province: province_global,
      country: country_global,
    });
  }, [first_name_global, last_name_global, email_global, phone_global, city_global,
    province_global, country_global ]);

  const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    update_profile(first_name, last_name, email, phone, city, province, country);
    history.push('/user/profile');
  };

  return (
    <Container fluid className="p-0">
      <Row>
        <Col>
          <Card>
            <CardHeader>
              <CardTitle tag="h5">
                Account Info
              </CardTitle>
            </CardHeader>
            <CardBody>
              <Form onSubmit={e => onSubmit(e)}>
                <Row form>
                  <Col md={6}>
                    <FormGroup>
                      <Label for="first_name">First Name</Label>
                      <Input
                        type="text"
                        name="first_name"
                        id="first_name"
                        placeholder={`${first_name_global}`}
                        onChange={e => onChange(e)}
                        value={first_name}
                      />
                    </FormGroup>
                  </Col>
                  <Col md={6}>
                    <FormGroup>
                      <Label for="last_name">Last Name</Label>
                      <Input
                        type="text"
                        name="last_name"
                        id="last_name"
                        placeholder={`${last_name_global}`}
                        onChange={e => onChange(e)}
                        value={last_name}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row form>
                  <Col md={6}>
                    <FormGroup>
                      <Label for="email">Email</Label>
                      <Input
                        type="text"
                        name="email"
                        id="email"
                        placeholder={`${email_global}`}
                        onChange={e => onChange(e)}
                        value={email}
                      />
                    </FormGroup>
                  </Col>
                  <Col md={6}>
                    <FormGroup>
                      <Label for="phone">Phone</Label>
                      <Input
                        type="text"
                        name="phone"
                        id="phone"
                        placeholder={`${phone_global}`}
                        onChange={e => onChange(e)}
                        value={phone}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row form>
                  <Col md={6}>
                    <FormGroup>
                      <Label for="city">City</Label>
                      <Input
                        type="text"
                        name="city"
                        id="city"
                        placeholder={`${city_global}`}
                        onChange={e => onChange(e)}
                        value={city}
                      />
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <FormGroup>
                      <Label for="province">Province</Label>
                      <Input
                        type="text"
                        name="province"
                        id="province"
                        placeholder={`${province_global}`}
                        onChange={e => onChange(e)}
                        value={province}
                      />
                    </FormGroup>
                  </Col>
                  <Col md={2}>
                    <FormGroup>
                      <Label id="country">Country</Label>
                      <Input
                        type="text"
                        name="country"
                        id="country"
                        placeholder={`${country_global}`}
                        onChange={e => onChange(e)}
                        value={country}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Button color="primary" type='submit'>Save Changes</Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  )
};

const mapStateToProps = state => ({
  username: state.profile.username,
  first_name_global: state.profile.first_name,
  last_name_global: state.profile.last_name,
  email_global: state.profile.email,
  phone_global: state.profile.phone,
  city_global: state.profile.city,
  province_global: state.profile.province,
  country_global: state.profile.country,
});

export default withRouter(connect(mapStateToProps, { update_profile })(EditProfile));
