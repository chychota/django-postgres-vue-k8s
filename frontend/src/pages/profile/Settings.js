import React, { useState } from "react";
import EditAccount from "./EditAccount";
import { delete_account } from "../../redux/actions/authActions";
import { connect } from "react-redux";
import {
  Button,
  Card,
  CardHeader,
  CardTitle,
  Col,
  Container,
  ListGroup,
  ListGroupItem,
  Row,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from "reactstrap";


const Navigation = ( { delete_account } ) => {
  const [isOpen, setOpen] = useState(false);

  const toggle = React.useCallback(() => {
    setOpen(value => !value);
  }, []);

  const [activeList, setActiveList] = useState("account")

  const activeAccount = () => {
    setActiveList("account")
  }

  const activeDelete = () => {
    setActiveList("delete")
    toggle()
  }



  return (
    <React.Fragment>
      <Col md="3" xl="2">
      <Card>
        <CardHeader>
          <CardTitle tag="h5" className="mb-0">
            Profile Settings
          </CardTitle>
        </CardHeader>
        <ListGroup flush>
          <ListGroupItem
            tag="a"
            href="#account"
            onClick={() => activeAccount()}
            action
            active={activeList === "account"}
          >
            Account
          </ListGroupItem>
          <ListGroupItem
            tag="a"
            href="#delete"
            onClick={() => activeDelete()}
            action
            active={ activeList === "delete" }
          >
            Delete Account
          </ListGroupItem>
        </ListGroup>
      </Card>
      <Modal isOpen={isOpen} centered>
        <ModalHeader>Delete Account</ModalHeader>
        <ModalBody className="text-center m-3">
          <p className="mb-0">
            Are you sure you want to delete your Account?
          </p>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={toggle}>Cancel</Button>{" "}
          <Button color="danger" onClick={delete_account}>
            Delete Account
          </Button>
        </ModalFooter>
      </Modal>
    </Col>
    <Col md="9" xl="10">
      {activeList === "account"?<EditAccount />:null}
    </Col>
    </React.Fragment>
  )
};


const Settings = ( { delete_account } ) => (
  <Container fluid className="p-0">
    <h1 className="h3 mb-3">Settings</h1>
    <Row>
      <Navigation delete_account={ delete_account }/>
    </Row>
  </Container>
);

export default connect(null,{ delete_account })(Settings);
