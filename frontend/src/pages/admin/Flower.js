import React from "react";
import Iframe from 'react-iframe'

const Flower = () => (
  <React.Fragment>
    <Iframe
      url={process.env.REACT_APP_FLOWER_URL}
      height="100%"
      width="100%"
    />
  </React.Fragment>
);

export default Flower;
