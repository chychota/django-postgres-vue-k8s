import React from "react";
import Iframe from 'react-iframe'

const RedisCommander = () => (
  <React.Fragment>
    <Iframe
      url={process.env.REACT_APP_REDIS_COMMANDER_URL}
      height="100%"
      width="100%"
    />
  </React.Fragment>
);

export default RedisCommander;
