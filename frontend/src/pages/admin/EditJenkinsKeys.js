import React, { useState, useEffect } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { update_jenkinskeys, delete_jenkinskey } from "../../redux/actions/keyActions";
import { CopyToClipboard } from "react-copy-to-clipboard";
import {
  Card,
  CardHeader,
  CardTitle,
  Container,
  Col,
  Row,
  Table,
  Input,
  Form,
  FormGroup,
  Button,
} from "reactstrap";

import {
  Copy,
  Trash,
} from "react-feather";

const Key = ({id, name, username, apikey, delete_jenkinskey }) => {
  const handleDelete = () => {
    delete_jenkinskey(id)
  };

  return(
    <React.Fragment>
      <tr>
        <td>{name}</td>
        <td>{username}</td>
        <td>{apikey}</td>
        <td>
          <CopyToClipboard text={apikey}>
            <Copy/>
          </CopyToClipboard>
          <span onClick={()=> handleDelete()}>
            <Trash color="red"/>
          </span>
        </td>
      </tr>
    </React.Fragment>
  )
};



const EditJenkinsKeys = ({ jenkins_keys_global,  update_jenkinskeys , delete_jenkinskey }) => {
  const [jenkinskeysData, setjenkinskeysData] = useState({
    jenkins_keys: jenkins_keys_global,
  });

  const [formData, setFormData] = useState({
    keyname: '',
    username: '',
    apikey: '',
  });

  const { keyname, username, apikey } = formData;

  const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit = e => {
    e.preventDefault();
    update_jenkinskeys(keyname, username, apikey)
  };

  useEffect(() => {
    setjenkinskeysData({
      jenkins_keys: jenkins_keys_global,
    });
  }, [jenkins_keys_global]);

  return(
    <Container fluid className="p-0">
      <Row>
        <Col>
          <Card>
            <CardHeader>
              <CardTitle tag="h5" className="mb-0">
                Jenkins Keys
              </CardTitle>
            </CardHeader>
            <Table striped>
              <thead>
              <tr>
                <th style={{ width: "20% "}}>Key Name</th>
                <th style={{ width: "20% "}}>User Name Server</th>
                <th style={{ width: "40% "}}>Key</th>
                <th style={{ width: "20% "}}>Actions</th>
              </tr>
              </thead>
              <tbody>
              {jenkinskeysData.jenkins_keys.map(({id, name, username, key}, index) => (
                <Key id={id} name={name} username={username} apikey={key} delete_jenkinskey={delete_jenkinskey} key={index}/>
              ))}
              </tbody>
            </Table>
            <Form onSubmit={e => onSubmit(e)}>
              <Table>
                <tbody>
                  <tr>
                    <td style={{ width: "20% "}}>
                      <FormGroup>
                        <Input
                          bsSize="lg"
                          type="text"
                          name="keyname"
                          placeholder="Enter Key Name"
                          onChange={e => onChange(e)}
                          value={keyname}
                        />
                      </FormGroup>
                    </td>
                    <td style={{ width: "20% "}}>
                      <FormGroup>
                        <Input
                          bsSize="lg"
                          type="text"
                          name="username"
                          placeholder="Username"
                          onChange={e => onChange(e)}
                          value={username}
                        />
                      </FormGroup>
                    </td>
                    <td style={{ width: "40% "}}>
                      <FormGroup>
                        <Input
                          bsSize="lg"
                          type="text"
                          name="apikey"
                          placeholder="Enter Key"
                          onChange={e => onChange(e)}
                          value={apikey}
                        />
                      </FormGroup>
                    </td>
                    <td style={{ width: "20% "}}>
                      <FormGroup>
                        <Button color="primary" size="lg" type='submit'>
                          Add Key
                        </Button>
                      </FormGroup>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Form>
          </Card>
        </Col>
      </Row>
    </Container>
  )
};

const mapStateToProps = state => ({
  user: state.profile.user,
  jenkins_keys_global: state.keys.jenkinskeys,
});

export default withRouter(connect(mapStateToProps, { update_jenkinskeys, delete_jenkinskey })(EditJenkinsKeys));
