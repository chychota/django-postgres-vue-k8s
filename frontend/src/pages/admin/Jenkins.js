import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import Select from "react-select";
import EditJenkinsKeys from "./EditJenkinsKeys";
import { update_jenkins, delete_jenkins, update_job, delete_job } from "../../redux/actions/serverActions"
import { Trash, CheckSquare, Square } from "react-feather";
import {
  Button,
  Card,
  CardHeader,
  CardTitle,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  ListGroup,
  ListGroupItem,
  Row,
  Table
} from "reactstrap";


const Server = ({id, name, hostname, port, ssl, certificatecheck, delete_jenkins }) => {
  const handleDelete = () => {
    delete_jenkins(id)
  };

  return(
    <React.Fragment>
      <tr>
        <td>{name}</td>
        <td>{hostname}</td>
        <td>{port}</td>
        <td>{ssl?<CheckSquare color="green"/>:<Square color="red"/>}</td>
        <td>{certificatecheck?<CheckSquare color="green"/>:<Square color="red"/>}</td>
        <td>
          <span onClick={()=> handleDelete()}>
            <Trash color="red"/>
          </span>
        </td>
      </tr>
    </React.Fragment>
  )
};


const Servers = ({jenkins_global, delete_jenkins, update_jenkins}) => {
  const [jenkinsServerData, setJenkinsServerData] = useState({
    jenkins: jenkins_global,
  });

  const [formData, setFormData] = useState({
    name: '',
    hostname: '',
    port: '80',
    ssl: false,
    certificatecheck: false,
  });

  const { name, hostname, port, ssl, certificatecheck } = formData;

  useEffect(() => {
    setJenkinsServerData({
      jenkins: jenkins_global,
    });
  }, [jenkins_global]);

  const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

  const onCheckChange = () => setFormData({ ...formData, certificatecheck: !certificatecheck });

  const onSSLChange = () => setFormData({ ...formData, ssl: !ssl });


  const onSubmit = e => {
    e.preventDefault();
    update_jenkins(name, hostname, port, ssl, certificatecheck)
  };


  return (
    <Container fluid className="p-0">
      <Row>
        <Col>
          <Card>
            <Table striped>
              <thead>
                <tr>
                  <th style={{ width: "15% "}}>Name</th>
                  <th style={{ width: "40% "}}>Hostname</th>
                  <th style={{ width: "10% "}}>Port</th>
                  <th style={{ width: "5% "}}>SSL</th>
                  <th style={{ width: "5% "}}>Cert Check</th>
                  <th style={{ width: "15% "}}>Action</th>
                </tr>
              </thead>
              <tbody>
              {jenkinsServerData.jenkins.map(({id, name, hostname, port, ssl, certificatecheck}, index) => (
                <Server id={id} name={name} hostname={hostname} port={port} ssl={ssl} certificatecheck={certificatecheck} delete_jenkins={delete_jenkins} key={index}/>
              ))}
              </tbody>
            </Table>
            <Form onSubmit={e => onSubmit(e)}>
              <Table>
                <tbody>
                <tr>
                  <td style={{ width: "15% "}}>
                    <FormGroup>
                      <Input
                        bsSize="lg"
                        type="text"
                        name="name"
                        placeholder="Enter Name"
                        onChange={e => onChange(e)}
                        value={name}
                      />
                    </FormGroup>
                  </td>
                  <td style={{ width: "40% "}}>
                    <FormGroup>
                      <Input
                        bsSize="lg"
                        type="text"
                        name="hostname"
                        placeholder="Enter Hostname"
                        onChange={e => onChange(e)}
                        value={hostname}
                      />
                    </FormGroup>
                  </td>
                  <td style={{ width: "10% "}}>
                    <FormGroup>
                      <Input
                        bsSize="lg"
                        type="text"
                        name="port"
                        placeholder={port}
                        onChange={e => onChange(e)}
                        value={port}
                      />
                    </FormGroup>
                  </td>
                  <td style={{ width: "5% "}}>
                    <FormGroup>
                      {ssl?<CheckSquare onClick={onSSLChange} color="green"/>:<Square onClick={onSSLChange} color="red"/>}
                    </FormGroup>
                  </td>
                  <td style={{ width: "5% "}}>
                    <FormGroup>
                      {certificatecheck?<CheckSquare onClick={onCheckChange} color="green"/>:<Square onClick={onCheckChange} color="red"/>}
                    </FormGroup>
                  </td>
                  <td style={{ width: "15% "}}>
                    <FormGroup>
                      <Button color="primary" size="lg" type='submit'>
                        Add Server
                      </Button>
                    </FormGroup>
                  </td>
                </tr>
                </tbody>
              </Table>
            </Form>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}


const Job = ({id, name, jenkins, delete_job }) => {
  const handleDelete = () => {
    delete_job(id)
  };

  return(
    <React.Fragment>
      <tr>
        <td>{name}</td>
        <td>{jenkins}</td>
        <td>
          <span onClick={()=> handleDelete()}>
            <Trash color="red"/>
          </span>
        </td>
      </tr>
    </React.Fragment>
  )
};

const Jobs = ({ jenkins_global, jobs_global, delete_job, update_job }) => {
  const [jobData, setJobData] = useState({
    jobs: jobs_global,
    jenkins: jenkins_global.map((jenkins) => ({ value: jenkins.id, label: jenkins.name })),
  });

  const [formData, setFormData] = useState({
    name: '',
    jenkins: null,
  });

  const { name, jenkins } = formData;

  useEffect(() => {
    setJobData({
      jobs: jobs_global,
      jenkins: jenkins_global.map((jenkins) => ({ value: jenkins.id, label: jenkins.name })),
    });
  }, [jobs_global, jenkins_global]);

  const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

  const onJenkinsChange = e => {
    setFormData({
      ...formData,
      jenkins: e.value
    });
  };

  const onSubmit = e => {
    e.preventDefault();
    update_job(name, jenkins)
  };

  return (
    <Container fluid className="p-0">
      <Row>
        <Col>
          <Card>
            <Table striped>
              <thead>
              <tr>
                <th style={{ width: "35% "}}>Name</th>
                <th style={{ width: "35% "}}>Jenkins</th>
                <th style={{ width: "30% "}}>Action</th>
              </tr>
              </thead>
              <tbody>
              {jobData.jobs.map(({id, name, jenkins}, index) => (
                <Job id={id} name={name} jenkins={jenkins.name} delete_job={delete_job} key={index}/>
              ))}
              </tbody>
            </Table>
            <Form onSubmit={e => onSubmit(e)}>
              <Table>
                <tbody>
                <tr>
                  <td style={{ width: "35% "}}>
                    <FormGroup>
                      <Input
                        bsSize="lg"
                        type="text"
                        name="name"
                        placeholder="Enter Name"
                        onChange={e => onChange(e)}
                        value={name}
                      />
                    </FormGroup>
                  </td>
                  <td style={{ width: "35% "}}>
                    <FormGroup>
                      <Select
                        className="react-select-container"
                        classNamePrefix="react-select"
                        options={jobData.jenkins}
                        onChange={e => onJenkinsChange(e)}
                      />
                    </FormGroup>
                  </td>
                  <td style={{ width: "30% "}}>
                    <FormGroup>
                      <Button color="primary" size="lg" type='submit'>
                        Add Job
                      </Button>
                    </FormGroup>
                  </td>
                </tr>
                </tbody>
              </Table>
            </Form>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}


const Navigation = ({jenkins_global, delete_jenkins, update_jenkins, jobs_global, delete_job, update_job}) => {
  const [activeList, setActiveList] = useState("servers")

  const activeServers = () => {
    setActiveList("servers")
  }

  const activeJobs = () => {
    setActiveList("jobs")
  }

  const activeKeys = () => {
    setActiveList("keys")
  }

  return (
    <React.Fragment>
      <Col md="3" xl="2">
        <Card>
          <CardHeader>
            <CardTitle tag="h5" className="mb-0">Jenkins Settings</CardTitle>
          </CardHeader>
          <ListGroup flush>
            <ListGroupItem
              tage="a"
              href="#servers"
              onClick={() => activeServers()}
              action
              active={activeList === "servers"}
            >
              Servers
            </ListGroupItem>
            <ListGroupItem
              tage="a"
              href="#jobs"
              onClick={() => activeJobs()}
              action
              active={activeList === "jobs"}
            >
              Jobs
            </ListGroupItem>
            <ListGroupItem
              tage="a"
              href="#keys"
              onClick={() => activeKeys()}
              action
              active={activeList === "keys"}
            >
              Keys
            </ListGroupItem>
          </ListGroup>
        </Card>
      </Col>
      <Col md="9" xl="10">
        {activeList === "servers"?<Servers jenkins_global={jenkins_global} delete_jenkins={delete_jenkins} update_jenkins={update_jenkins}/>:null}
        {activeList === "jobs"?<Jobs jenkins_global={jenkins_global} jobs_global={jobs_global} delete_job={delete_job} update_job={update_job}/>:null}
        {activeList === "keys"?<EditJenkinsKeys />:null}
      </Col>
    </React.Fragment>
  )
}

const ManageJenkins = ({jenkins_global, delete_jenkins, update_jenkins, jobs_global, delete_job, update_job}) => (
  <Container fluid className="p-0">
    <h1 className="h3 mb-3">Manage Jenkins</h1>
    <Row>
      <Navigation
        jenkins_global={jenkins_global}
        delete_jenkins={delete_jenkins}
        update_jenkins={update_jenkins}
        jobs_global={jobs_global}
        delete_job={delete_job}
        update_job={update_job}
      />
    </Row>
  </Container>
);

const mapStateToProps = state => ({
  jenkins_global: state.servers.jenkins,
  jobs_global: state.servers.jobs,
});

export default connect(mapStateToProps, {update_jenkins, delete_jenkins, update_job, delete_job})(ManageJenkins);
