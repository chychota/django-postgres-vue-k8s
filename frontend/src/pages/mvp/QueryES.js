import React, { useState, useEffect } from "react";
import Select from "react-select";
import { connect } from "react-redux";
import { query_es, poll_result } from "../../redux/actions/serverActions";
import { Bar } from "react-chartjs-2";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Container,
  Row,
  FormGroup,
  Label,
  Button,
  Form
} from "reactstrap";

const QueryES = ({ es_global, query_id_global, query_result_global, query_es, poll_result }) => {

  const [esData, setesData] = useState({
    es: es_global.map((es) => ({ value: es.id, label: es.name })),
    query_id: query_id_global,
    query_result: query_result_global,
  });

  const [selectedOptions, setSelectedOptions] = useState({
    es_selected: null,
  });

  useEffect(() => {
    setesData({
      es: es_global.map((es) => ({ value: es.id, label: es.name })),
      query_id: query_id_global,
      query_result: query_result_global,
    });
  }, [es_global, query_id_global, query_result_global]);

  useEffect(() => {
    poll_result(query_id_global)
  }, [poll_result, query_id_global]);

  const onESChange = e => {
    setSelectedOptions({
      es_selected: e.value,
    });
  };


  const onSubmit = (e) => {
    e.preventDefault();
    query_es(selectedOptions.es_selected);
  }

  const data = {
    labels: [
      "Hits",
    ],
    datasets: [
      {
        label: "Total",
        backgroundColor: "primary",
        borderColor:"primary",
        hoverBackgroundColor: "primary",
        hoverBorderColor: "primary",
        data: [esData.query_result],
        barPercentage: 0.75,
        categoryPercentage: 0.5
      }
    ]
  };

    const options = {
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    scales: {
      yAxes: [
        {
          gridLines: {
            display: false
          },
          stacked: false,
          ticks: {
            stepSize: 20
          }
        }
      ],
      xAxes: [
        {
          stacked: false,
          gridLines: {
            color: "transparent"
          }
        }
      ]
    }
  };

  return(
    <Container fluid className="p-0">
      <h1 className="h3 mb-3">Query Elasticsearch</h1>
      <Row>
        <Col>
          <Card>
            <CardHeader>
              <CardTitle tag="h5" className="mb-0">Tests Results</CardTitle>
            </CardHeader>
            <CardBody>
              <Form id="form" onSubmit={e => onSubmit(e)}>
                <Row>
                  <Col lg="4">
                    <FormGroup>
                      <Label>Select Elasticsearch</Label>
                      <Select
                        className="react-select-container"
                        classNamePrefix="react-select"
                        options={esData.es}
                        onChange={e => onESChange(e)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Button color="primary" type='submit'>Run</Button>
              </Form>
            </CardBody>
          </Card>
          <Card>
            <CardHeader>
              <CardTitle tag="h5">Hit Chart</CardTitle>
              <h6 className="card-subtitle text-muted">
              The number of your Elasticsearch hits: {esData.query_result}
              </h6>
            </CardHeader>
            <CardBody>
              <div className="chart">
                <Bar data={data} options={options} />
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  )
};

const mapStateToProps = state => ({
  es_global: state.servers.es,
  query_id_global: state.servers.query_id,
  query_result_global: state.servers.query_result,
});

export default connect(mapStateToProps, { query_es, poll_result })(QueryES);



