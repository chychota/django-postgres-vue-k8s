import React from 'react';
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import {
  Col,
  Row
} from "reactstrap";


const Dashboard = ({ isAuthenticated, first_name }) => {
  const noUsername = (
    <React.Fragment>
      Welcome!
    </React.Fragment>
  );

  const hasUsername = (
    <React.Fragment>
      Welcome back, {first_name}!
    </React.Fragment>
  );

  return (
    isAuthenticated ? (
      <React.Fragment>
        <Row className="mb-2 mb-xl-4">
          <Col xs="auto" className="d-none d-sm-block">
            <h1 className="my-4 font-weight-normal">
              { first_name ? hasUsername : noUsername}
            </h1>
            <p className="text-muted lead">
              Automation Manager
            </p>
          </Col>
        </Row>
      </React.Fragment>
    ) : (
       <Redirect to='/' />
    )
  )
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  first_name: state.profile.first_name
});

export default connect(mapStateToProps)(Dashboard);
