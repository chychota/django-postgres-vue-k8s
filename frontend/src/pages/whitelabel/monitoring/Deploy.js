import React, {useState} from "react";
import Select from "react-select";
import { connect } from "react-redux";
import { run_job, add_parameter } from "../../../redux/actions/serverActions";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from "reactstrap";

const Deploy = ({ issuer, run_job, add_parameter}) => {
  const [formData, setFormData] = useState({
    target: '',
    clustername: 'test',
    namespace: 'whitelabel',
    branch: 'master',
  });

  const {
    target,
    clustername,
    namespace,
    branch
  } = formData;

  const onChange = e => setFormData({
      ...formData,
      [e.target.name]: e.target.value
    }
  );

  const onTGChange = e => setFormData({
      ...formData,
      target: e.value
    }
  );

  const onSubmit = e => {
    e.preventDefault();
    console.log("Target: ", target);
    console.log("Cluster Name: ", clustername);
    console.log("Namespace: ", namespace);
    console.log("Branch: ", branch);
    run_job('5fd43baf-0977-4e47-b17f-d109e75bab16', 'b11ec791-e414-4544-aace-4b9f240f3290', issuer).then((value) => {
      console.log(value.id);
      add_parameter('Target', target, value.id);
      add_parameter('ClusterName', clustername, value.id);
      add_parameter('Namespace', namespace, value.id);
      add_parameter('Branch', branch, value.id);
    });
  };

  const options = [
    {value: 'development', label: 'Development'},
    {value: 'production', label: 'Production'}
  ]
  return (
    <Container fluid className="p-0">
      <h1 className="h3 mb-3">White Label Amazon Web Services</h1>
      <Row>
        <Col lg="12">
          <Card>
            <CardHeader>
              <CardTitle tag="h5">Dispose Amazon Monitoring</CardTitle>
            </CardHeader>
            <CardBody>
              <Form onSubmit={e => onSubmit(e)}>
                <FormGroup>
                  <Label>Target</Label>
                  <Select
                    className="react-select-container"
                    classNamePrefix="react-select"
                    options={options}
                    onChange={e => onTGChange(e)}
                  />
                </FormGroup>

                <FormGroup>
                  <Label>Cluster Name</Label>
                  <Input
                    type="text"
                    name="clustername"
                    placeholder="test"
                    onChange={e => onChange(e)}
                    value={clustername}
                  />
                </FormGroup>


                <FormGroup>
                  <Label>Namespace</Label>
                  <Input
                    type="text"
                    name="namespace"
                    placeholder="whitelabel"
                    onChange={e => onChange(e)}
                    value={namespace}
                  />
                </FormGroup>

                <FormGroup>
                  <Label>Branch</Label>
                  <Input
                    type="text"
                    name="branch"
                    placeholder="master"
                    onChange={e => onChange(e)}
                    value={branch}
                  />
                </FormGroup>


                <Button color="primary">Submit</Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  )
};


const mapStateToProps = state => ({
  issuer: state.profile.user,
});

export default connect(mapStateToProps, { run_job, add_parameter })(Deploy);
