import React, {useState} from "react";
import Select from "react-select";
import { connect } from "react-redux";
import { run_job, add_parameter } from "../../../redux/actions/serverActions";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Container,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from "reactstrap";

const Dispose = ({ issuer, run_job, add_parameter}) => {
  const [formData, setFormData] = useState({
    target: '',
    clustername: 'test',
    namespace: 'whitelabel',
  });

  const {
    target,
    clustername,
    namespace
  } = formData;

  const onChange = e => setFormData({
      ...formData,
      [e.target.name]: e.target.value
    }
  );

  const onTGChange = e => setFormData({
      ...formData,
      target: e.value
    }
  );

  const onSubmit = e => {
    e.preventDefault();
    console.log("Target: ", target);
    console.log("Cluster Name: ", clustername);
    console.log("Namespace: ", namespace);
    run_job('fa10643a-520e-4842-84f8-0d8bac4f7e15', 'b11ec791-e414-4544-aace-4b9f240f3290', issuer).then((value) => {
      console.log(value.id);
      add_parameter('Target', target, value.id);
      add_parameter('ClusterName', clustername, value.id);
      add_parameter('Namespace', namespace, value.id);
    });
  };

  const options = [
    {value: 'development', label: 'Development'},
    {value: 'production', label: 'Production'}
  ]
  return (
    <Container fluid className="p-0">
      <h1 className="h3 mb-3">White Label Amazon Web Services</h1>
      <Row>
        <Col lg="12">
          <Card>
            <CardHeader>
              <CardTitle tag="h5">Dispose Amazon Monitoring</CardTitle>
            </CardHeader>
            <CardBody>
              <Form onSubmit={e => onSubmit(e)}>
                <FormGroup>
                  <Label>Target</Label>
                  <Select
                    className="react-select-container"
                    classNamePrefix="react-select"
                    options={options}
                    onChange={e => onTGChange(e)}
                  />
                </FormGroup>

                <FormGroup>
                  <Label>Cluster Name</Label>
                  <Input
                    type="text"
                    name="clustername"
                    placeholder="test"
                    onChange={e => onChange(e)}
                    value={clustername}
                  />
                </FormGroup>


                <FormGroup>
                  <Label>Namespace</Label>
                  <Input
                    type="text"
                    name="namespace"
                    placeholder="whitelabel"
                    onChange={e => onChange(e)}
                    value={namespace}
                  />
                </FormGroup>


                <Button color="primary">Submit</Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  )
};


const mapStateToProps = state => ({
  issuer: state.profile.user,
});

export default connect(mapStateToProps, { run_job, add_parameter })(Dispose);
