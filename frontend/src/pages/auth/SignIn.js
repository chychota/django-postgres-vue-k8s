import React, {useState} from "react";
import { Link, Redirect } from "react-router-dom";
import {connect} from "react-redux";
import { login } from "../../redux/actions/authActions";
import CSRFToken from "../../components/CSRFToken";
import {
  Button,
  Card,
  CardBody,
  Form,
  FormGroup,
  Label,
  Input
} from "reactstrap";

import avatar from "../../assets/img/avatars/avatar.png";

const SignIn = ({ login, isAuthenticated }) => {
  const [formData, setFormData] = useState({
    username: '',
    password: ''
  });
  const [keyCapOn, setKeyCapOn] = useState(false);
  const { username, password } = formData;

  const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

  const onKeyDown = keyEvent => {
    if (keyEvent.getModifierState("CapsLock")) {
      setKeyCapOn(true)
    } else {
      setKeyCapOn(false)
    }
  };

  const onSubmit = e => {
    e.preventDefault();
    login(username, password);
  };

  if (isAuthenticated)
    return <Redirect to='/' />;

  return (
    <React.Fragment>
      <div className="text-center mt-4">
        <h1 className="h2">Automation Manager</h1>
        <p className="lead">
          Sign in with your Nokia account to continue
        </p>
      </div>

      <Card>
        <CardBody>
          <div className="m-sm-4">
            <div className="text-center">
              <img
                src={avatar}
                alt="avatar"
                className="img-fluid"
                width="132"
                height="132"
              />
            </div>
            <Form onSubmit={e => onSubmit(e)}>
              <CSRFToken />
              <FormGroup>
                <Label>Username</Label>
                <Input
                  bsSize="lg"
                  type="text"
                  name="username"
                  placeholder="Enter your username"
                  onChange={e => onChange(e)}
                  value={username}
                  required
                />
              </FormGroup>
              <FormGroup>
                <Label>Password</Label>
                <Input
                  bsSize="lg"
                  type="password"
                  name="password"
                  placeholder="Enter your password"
                  onChange={e => onChange(e)}
                  value={password}
                  required
                  onKeyDown={(keyEvent) => onKeyDown(keyEvent)}
                />
                {keyCapOn && <small>Caps Lock On!</small>}
              </FormGroup>
              <div className="text-center mt-3">
                  <Button color="primary" size="lg" type='submit'>
                    Sign in
                  </Button>
              </div>
            </Form>
            <small>
              Don't have an account? <Link to="/auth/sign-up">Sign Up</Link>
            </small>
          </div>
        </CardBody>
      </Card>
    </React.Fragment>
  )
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { login } )(SignIn);
