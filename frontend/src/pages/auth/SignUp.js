import React, { useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { register } from "../../redux/actions/authActions";
import CSRFToken from "../../components/CSRFToken";
import { toastr } from "react-redux-toastr";
import {
  Button,
  Card,
  CardBody,
  Form,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import avatar from "../../assets/img/avatars/avatar.png";

const SignUp = ({ register , isAuthenticated }) => {
  const [formData, setFormData] = useState({
    username: '',
    password: '',
    re_password: ''
  });
  const [accountCreated, setAccountCreated] = useState(false);
  const [keyCapOn, setKeyCapOn] = useState(false);
  const { username, password, re_password } = formData;

  const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

  const onKeyDown = keyEvent => {
    if (keyEvent.getModifierState("CapsLock")) {
      setKeyCapOn(true)
    } else {
      setKeyCapOn(false)
    }
  };

  const onSubmit = e => {
    e.preventDefault();

    if (password === re_password) {
      register(username, password, re_password);
      setAccountCreated(true)
    } else {
      toastr.error("Passwords to not match.")
    }
  };

  if (isAuthenticated)
    return <Redirect to='/' />;
  else if (accountCreated)
      return <Redirect to='/auth/sign-in' />;

  return (
    <React.Fragment>
      <div className="text-center mt-4">
        <h1 className="h2">Automation Manager</h1>
        <p className="lead">
          Create local credentials (not a Nokia user)
        </p>
      </div>

      <Card>
        <CardBody>
          <div className="m-sm-4">
            <div className="text-center">
              <img
                src={avatar}
                alt="avatar"
                className="img-fluid"
                width="132"
                height="132"
              />
            </div>
            <Form onSubmit={e => onSubmit(e)}>
              <CSRFToken />
              <FormGroup>
                <Label>Username</Label>
                <Input
                  bsSize="lg"
                  type="text"
                  name="username"
                  placeholder="Enter a username"
                  onChange={e => onChange(e)}
                  value={username}
                  required
                />
              </FormGroup>
              <FormGroup>
                <Label>Password</Label>
                <Input
                  bsSize="lg"
                  type="password"
                  name="password"
                  placeholder="Enter password"
                  onChange={e => onChange(e)}
                  value={password}
                  minLength='6'
                  required
                  onKeyDown={(keyEvent) => onKeyDown(keyEvent)}
                />
                {keyCapOn && <small>Caps Lock On!</small>}
              </FormGroup>
              <FormGroup>
                <Label>Confirm Password</Label>
                <Input
                  bsSize="lg"
                  type="password"
                  name="re_password"
                  placeholder="Confirm password"
                  onChange={e => onChange(e)}
                  value={re_password}
                  minLength='6'
                  required
                  onKeyDown={(keyEvent) => onKeyDown(keyEvent)}
                />
                {keyCapOn && <small>Caps Lock On!</small>}
              </FormGroup>
              <div className="text-center mt-3">
                <Button color="primary" size="lg" type='submit'>
                  Sign up
                </Button>
              </div>
            </Form>
            <small>
              All ready have an account? <Link to="/auth/sign-in">Sign In</Link>
            </small>
          </div>
        </CardBody>
      </Card>
    </React.Fragment>
  )
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, { register } )(SignUp);
