import {
  LOAD_USER_PROFILE_SUCCESS,
  LOAD_USER_PROFILE_FAILURE,
  UPDATE_USER_PROFILE_SUCCESS,
  UPDATE_USER_PROFILE_FAILURE,
  LOGIN_KNOX_SUCCESS,
  LOGIN_KNOX_FAILURE,
  UPDATE_USER_TOKEN_SUCCESS,
  UPDATE_USER_TOKEN_FAILURE
} from "../constants";


const initialState = {
  username: '',
  first_name: '',
  last_name: '',
  email: '',
  phone: '',
  city: '',
  province: '',
  country: '',
  portal_knox_token: '',
  isAdmin: false,
  sats_role: 'GU',
  cats_role: 'GU',
  pats_role: 'GU',
  oats_role: 'GU',
  dats_role: 'GU',
  wfm_role: 'GU',
  devops_role: 'GU',
  user: '',
};

export default function(state= initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case LOAD_USER_PROFILE_SUCCESS:
    case UPDATE_USER_PROFILE_SUCCESS:
    case UPDATE_USER_TOKEN_SUCCESS:
      return {
        ...state,
        username: payload.username,
        first_name: payload.profile.first_name,
        last_name: payload.profile.last_name,
        email: payload.profile.email,
        phone: payload.profile.phone,
        city: payload.profile.city,
        province: payload.profile.province,
        country: payload.profile.country,
        portal_knox_token: payload.profile.portal_knox_token,
        isAdmin: payload.profile.isAdmin,
        sats_role: payload.profile.sats_role,
        cats_role: payload.profile.cats_role,
        pats_role: payload.profile.pats_role,
        oats_role: payload.profile.oats_role,
        dats_role: payload.profile.dats_role,
        wfm_role: payload.profile.wfm_role,
        devops_role: payload.profile.devops_role,
        user: payload.profile.user,
      };
    case LOAD_USER_PROFILE_FAILURE:
      return {
        ...state,
        username: '',
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
        city: '',
        province: '',
        country: '',
        portal_knox_token: '',
        isAdmin: false,
        sats_role: 'GU',
        cats_role: 'GU',
        pats_role: 'GU',
        oats_role: 'GU',
        dats_role: 'GU',
        wfm_role: 'GU',
        devops_role: 'GU',
        user: '',
      };
    case LOGIN_KNOX_SUCCESS:
      return {
        ...state,
        portal_knox_token: payload.token,
      };
    case UPDATE_USER_PROFILE_FAILURE:
    case LOGIN_KNOX_FAILURE:
    case UPDATE_USER_TOKEN_FAILURE:
      return {
        ...state
      };
    default:
      return state;
  }
}
