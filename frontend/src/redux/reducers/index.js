import { combineReducers } from "redux";

import auth from "./authReducers";
import profile from "./profileReducers";
import sidebar from "./sidebarReducers";
import servers from "./serverReducers";
import keys from "./keyReducers";

import { reducer as toastr } from "react-redux-toastr";

const rootReducer = combineReducers({
  auth,
  profile,
  sidebar,
  servers,
  keys,
  toastr
});

export default rootReducer;
