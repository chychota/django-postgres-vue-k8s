import {
  LOAD_JENKINS_KEYS_SUCCESS,
  LOAD_JENKINS_KEYS_FAILURE,
  UPDATE_JENKINS_KEYS_SUCCESS,
  UPDATE_JENKINS_KEYS_FAILURE,
  DELETE_JENKINS_KEY_SUCCESS,
  DELETE_JENKINS_KEY_FAILURE,
  LOAD_TESTLINK_KEYS_SUCCESS,
  LOAD_TESTLINK_KEYS_FAILURE,
  UPDATE_TESTLINK_KEYS_SUCCESS,
  UPDATE_TESTLINK_KEYS_FAILURE
} from "../constants";

const initialState = {
  jenkinskeys: [],
  testlinkkeys: [],
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case LOAD_JENKINS_KEYS_SUCCESS:
      return {
        ...state,
        jenkinskeys: payload
      };
    case LOAD_TESTLINK_KEYS_SUCCESS:
      return {
        ...state,
        testlinkkeys: payload
      };
    case LOAD_JENKINS_KEYS_FAILURE:
    case UPDATE_JENKINS_KEYS_SUCCESS:
    case UPDATE_JENKINS_KEYS_FAILURE:
    case DELETE_JENKINS_KEY_SUCCESS:
    case DELETE_JENKINS_KEY_FAILURE:
    case LOAD_TESTLINK_KEYS_FAILURE:
    case UPDATE_TESTLINK_KEYS_SUCCESS:
    case UPDATE_TESTLINK_KEYS_FAILURE:
    default:
      return state;
  }
}
