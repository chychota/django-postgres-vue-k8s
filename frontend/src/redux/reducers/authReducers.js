import {
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  AUTHENTICATED_SUCCESS,
  AUTHENTICATED_FAILURE,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILURE
} from "../constants";


const initialState = {
  isAuthenticated: false
};

export default function(state= initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case AUTHENTICATED_SUCCESS:
    case AUTHENTICATED_FAILURE:
      return {
        ...state,
        isAuthenticated: payload
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true
      };
    case LOGOUT_SUCCESS:
    case DELETE_USER_SUCCESS:
      return {
        ...state,
        isAuthenticated: false
      };

    case REGISTER_SUCCESS:
      return {
        ...state,
        isAuthenticated: false
      };
    case LOGIN_FAILURE:
    case LOGOUT_FAILURE:
    case REGISTER_FAILURE:
    case DELETE_USER_FAILURE:
      return state;
    default:
      return state;
  }
}
