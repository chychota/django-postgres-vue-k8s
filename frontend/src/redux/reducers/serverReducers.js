import {
  LOAD_JENKINS_SERVERS_SUCCESS,
  LOAD_JENKINS_SERVERS_FAILURE,
  LOAD_JENKINS_JOBS_SUCCESS,
  LOAD_JENKINS_JOBS_FAILURE,
  RUN_JENKINS_JOB_SUCCESS,
  RUN_JENKINS_JOB_FAILURE,
  LOAD_ES_SERVERS_SUCCESS,
  LOAD_ES_SERVERS_FAILURE,
  SUBMIT_ES_QUERY_SUCCESS,
  SUBMIT_ES_QUERY_FAILURE,
  POLL_RESULT_SUCCESS,
  POLL_RESULT_FAILURE,
  UPDATE_JENKINS_SUCCESS,
  UPDATE_JENKINS_FAILURE,
  DELETE_JENKINS_SUCCESS,
  DELETE_JENKINS_FAILURE,
  UPDATE_JENKINS_JOB_SUCCESS,
  UPDATE_JENKINS_JOB_FAILURE,
  DELETE_JENKINS_JOB_SUCCESS,
  DELETE_JENKINS_JOB_FAILURE,
  ADD_JENKINS_JOB_PARAMETER_SUCCESS,
  ADD_JENKINS_JOB_PARAMETER_FAILURE,
} from "../constants";

const initialState = {
  jenkins: [],
  jobs: [],
  es: [],
  query_id: "",
  query_result: 0,
};

export default function reducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case LOAD_JENKINS_SERVERS_SUCCESS:
      return {
        ...state,
        jenkins: payload
      };
    case LOAD_JENKINS_JOBS_SUCCESS:
      return {
        ...state,
        jobs: payload
      };
    case LOAD_ES_SERVERS_SUCCESS:
      return {
        ...state,
        es: payload
      };
    case SUBMIT_ES_QUERY_SUCCESS:
      return {
        ...state,
        query_id: payload.task_id
      }
    case SUBMIT_ES_QUERY_FAILURE:
      return {
        ...state,
        query_id: ""
      }
    case POLL_RESULT_SUCCESS:
      return {
        ...state,
        query_result: payload.hits
      }
    case POLL_RESULT_FAILURE:
            return {
        ...state,
        query_result: 0
      }
    case DELETE_JENKINS_SUCCESS:
    case DELETE_JENKINS_FAILURE:
    case UPDATE_JENKINS_SUCCESS:
    case UPDATE_JENKINS_FAILURE:
    case UPDATE_JENKINS_JOB_SUCCESS:
    case UPDATE_JENKINS_JOB_FAILURE:
    case DELETE_JENKINS_JOB_SUCCESS:
    case DELETE_JENKINS_JOB_FAILURE:
    case LOAD_JENKINS_SERVERS_FAILURE:
    case LOAD_JENKINS_JOBS_FAILURE:
    case RUN_JENKINS_JOB_SUCCESS:
    case RUN_JENKINS_JOB_FAILURE:
    case LOAD_ES_SERVERS_FAILURE:
    case ADD_JENKINS_JOB_PARAMETER_SUCCESS:
    case ADD_JENKINS_JOB_PARAMETER_FAILURE:
      return state;
    default:
      return state;
  }
}
