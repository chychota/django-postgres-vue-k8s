import Cookies from 'js-cookie';
import axios from 'axios';
import {
  LOAD_JENKINS_KEYS_SUCCESS,
  LOAD_JENKINS_KEYS_FAILURE,
  LOAD_TESTLINK_KEYS_SUCCESS,
  LOAD_TESTLINK_KEYS_FAILURE,
  UPDATE_JENKINS_KEYS_SUCCESS,
  UPDATE_JENKINS_KEYS_FAILURE,
  DELETE_JENKINS_KEY_SUCCESS,
  DELETE_JENKINS_KEY_FAILURE
} from "../constants";

export const load_jenkins_keys = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  try {
    const res = await axios.get('/servers/jenkinskeys/', config);

    if (res.data.detail) {
      dispatch({
        type: LOAD_JENKINS_KEYS_FAILURE
      });
    } else {
      dispatch({
        type: LOAD_JENKINS_KEYS_SUCCESS,
        payload: res.data
      });
    }
  } catch (err) {
    dispatch({
      type: LOAD_JENKINS_KEYS_FAILURE
    });
  }
};

export const load_testlink_keys = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  try {
    const res = await axios.get('/servers/testlinkkeys/', config);

    if (res.data.detail) {
      dispatch({
        type: LOAD_TESTLINK_KEYS_FAILURE
      });
    } else {
      dispatch({
        type: LOAD_TESTLINK_KEYS_SUCCESS,
        payload: res.data
      });
    }
  } catch (err) {
    dispatch({
      type: LOAD_TESTLINK_KEYS_FAILURE
    });
  }
};

export const update_jenkinskeys = (name, username, key) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true,
    name,
    username,
    key
  });

  try {
    const res = await axios.post('/servers/jenkinskeys/', body, config);

    if (res.data.detail) {
      dispatch({
        type: UPDATE_JENKINS_KEYS_FAILURE
      });
    } else {
      dispatch({
        type: UPDATE_JENKINS_KEYS_SUCCESS
      });
      dispatch(load_jenkins_keys());
    }
  } catch (err) {
    dispatch({
      type: UPDATE_JENKINS_KEYS_FAILURE
    });
  }
};

export const delete_jenkinskey = (keyid) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true,
  });


  const url = "/servers/jenkinskeys/" + keyid + "/"

  try {
    const res = await axios.delete(url, config, body);

    if ( res.status >= 200 && res.status < 300) {
      dispatch({
        type: DELETE_JENKINS_KEY_SUCCESS
      });
      dispatch(load_jenkins_keys());
    } else {
      dispatch({
        type: DELETE_JENKINS_KEY_FAILURE
      });
    }
  } catch (err) {
    dispatch({
      type: DELETE_JENKINS_KEY_FAILURE
    });
    console.log('I am here')
  }
};
