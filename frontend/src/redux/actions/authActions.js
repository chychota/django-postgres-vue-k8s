import Cookies from 'js-cookie';
import { toastr } from "react-redux-toastr";
import axios from 'axios';
import { load_user } from "./profileActions";
import {
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
  AUTHENTICATED_SUCCESS,
  AUTHENTICATED_FAILURE,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILURE
} from "../constants";

export const checkAuthenticated = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  try {
    const res = await axios.get('/accounts/authenticated', config);

    if (res.data.error || res.data.isAuthenticated === 'error') {
      dispatch({
        type: AUTHENTICATED_FAILURE,
        payload: false
      });
    }
    else if (res.data.isAuthenticated === 'success') {
      dispatch({
        type: AUTHENTICATED_SUCCESS,
        payload: true
      });
    }
    else {
      dispatch({
        type: AUTHENTICATED_FAILURE,
        payload: false
      });
    }
  } catch (err) {
      dispatch({
        type: AUTHENTICATED_FAILURE,
        payload: false
      });
  }
};


export const login = (username, password) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({ username, password });

  try {
    const res = await axios.post('/accounts/login', body, config);

    if (res.data.success) {
      dispatch({
        type: LOGIN_SUCCESS
      });
      dispatch(load_user());
    } else {
      dispatch({
        type: LOGIN_FAILURE
      });
      toastr.error(res.data.error);
    }
  } catch (err) {
    dispatch({
      type: LOGIN_FAILURE
    });
  }
};

export const logout = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true
  });

  try {
    const res = await axios.post('/accounts/logout', body, config);

    if (res.data.success) {
      dispatch({
        type: LOGOUT_SUCCESS
      });
    } else {
      dispatch({
        type: LOGOUT_FAILURE
      });
      toastr.error(res.data.error);
    }
  } catch (err) {
    dispatch({
      type: LOGOUT_FAILURE
    });
  }
};

export const register = (username, password, re_password) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({ username, password, re_password });

  try {
    const res = await axios.post('/accounts/register', body, config);

    if (res.data.error) {
      dispatch({
        type: REGISTER_FAILURE
      });
      toastr.error(res.data.error);
    } else {
      dispatch({
        type: REGISTER_SUCCESS
      });
      toastr.success(res.data.success);
    }
  } catch (err) {
    dispatch({
      type: REGISTER_FAILURE
    });
  }
};

export const delete_account = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true
  });

  try {
    const res = await axios.delete('/accounts/delete', config, body);

    if (res.data.success) {
      dispatch({
        type: DELETE_USER_SUCCESS
      });
    }
    else {
      dispatch({
        type: DELETE_USER_FAILURE
      });
    }
  } catch (err) {
    dispatch({
      type: DELETE_USER_FAILURE
    });
  }
};
