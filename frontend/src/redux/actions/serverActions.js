import Cookies from 'js-cookie';
import axios from 'axios';
import {
  LOAD_JENKINS_SERVERS_SUCCESS,
  LOAD_JENKINS_SERVERS_FAILURE,
  LOAD_JENKINS_JOBS_SUCCESS,
  LOAD_JENKINS_JOBS_FAILURE,
  RUN_JENKINS_JOB_SUCCESS,
  RUN_JENKINS_JOB_FAILURE,
  LOAD_ES_SERVERS_SUCCESS,
  LOAD_ES_SERVERS_FAILURE,
  SUBMIT_ES_QUERY_SUCCESS,
  SUBMIT_ES_QUERY_FAILURE,
  POLL_RESULT_SUCCESS,
  POLL_RESULT_FAILURE,
  UPDATE_JENKINS_SUCCESS,
  UPDATE_JENKINS_FAILURE,
  DELETE_JENKINS_SUCCESS,
  DELETE_JENKINS_FAILURE,
  UPDATE_JENKINS_JOB_SUCCESS,
  UPDATE_JENKINS_JOB_FAILURE,
  DELETE_JENKINS_JOB_SUCCESS,
  DELETE_JENKINS_JOB_FAILURE,
  ADD_JENKINS_JOB_PARAMETER_SUCCESS,
  ADD_JENKINS_JOB_PARAMETER_FAILURE,
} from "../constants";


export const load_jenkins = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  try {
    const res = await axios.get('/servers/jenkins', config);

    if (res.data.detail) {
      dispatch({
        type: LOAD_JENKINS_SERVERS_FAILURE
      });
    } else {
      dispatch({
        type: LOAD_JENKINS_SERVERS_SUCCESS,
        payload: res.data
      });
    }
  } catch (err) {
    dispatch({
      type: LOAD_JENKINS_SERVERS_FAILURE
    });
  }
};

export const load_jobs = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  const url = '/servers/jobs/'


  try {
    const res = await axios.get(url, config);

    if (res.data.detail) {
      dispatch({
        type: LOAD_JENKINS_JOBS_FAILURE
      });
    } else {
      dispatch({
        type: LOAD_JENKINS_JOBS_SUCCESS,
        payload: res.data
      });
    }
  } catch (err) {
    dispatch({
      type: LOAD_JENKINS_JOBS_FAILURE
    });
  }
};

export const run_job = ( job, key, issuer ) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true,
    job,
    key,
    issuer,
  });

  try {
    const res = await axios.post('/servers/runjob/', body, config);

    if (res.data.detail) {
      dispatch({
        type: RUN_JENKINS_JOB_FAILURE
      });
    } else {
      dispatch({
        type: RUN_JENKINS_JOB_SUCCESS
      });
      return res.data
    }
  } catch (err) {
    dispatch({
      type: RUN_JENKINS_JOB_FAILURE
    });
  }
};


export const add_parameter = ( name, value, jobrun ) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true,
    name,
    value,
    jobrun,
  });

  try {
    const res = await axios.post('/servers/parameter/', body, config);

    if ( res.status >= 200 && res.status < 300) {
      dispatch({
        type: ADD_JENKINS_JOB_PARAMETER_SUCCESS
      });
    } else {
      dispatch({
        type: ADD_JENKINS_JOB_PARAMETER_FAILURE
      });
    }
  } catch (err) {
    dispatch({
      type: ADD_JENKINS_JOB_PARAMETER_FAILURE
    });
  }
};


export const load_es = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  try {
    const res = await axios.get('/servers/elasticsearch/', config);

    if (res.data.detail) {
      dispatch({
        type: LOAD_ES_SERVERS_FAILURE
      });
    } else {
      dispatch({
        type: LOAD_ES_SERVERS_SUCCESS,
        payload: res.data
      });
    }
  } catch (err) {
    dispatch({
      type: LOAD_ES_SERVERS_FAILURE
    });
  }
};


export const query_es = (es_id) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  const url = '/api/v1/elasticsearch/query/' + es_id + '/'

  try {
    const res = await axios.get(url, config);

    if ( res.status === 202) {
      dispatch({
        type: SUBMIT_ES_QUERY_SUCCESS,
        payload: res.data
      });
    } else {
      dispatch({
        type: SUBMIT_ES_QUERY_FAILURE
      });
    }
  } catch (err) {
    dispatch({
      type: SUBMIT_ES_QUERY_FAILURE
    });
  }
};

export const poll_result = (task_id) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  const url = '/api/v1/poll_async_results/' + task_id + '/'

  const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }
  await sleep(1000)

  try {
    const res = await axios.get(url, config);

    if ( res.status === 200) {
      dispatch({
        type:  POLL_RESULT_SUCCESS,
        payload: res.data
      });
    } else {
      dispatch({
        type:  POLL_RESULT_FAILURE
      });
    }
  } catch (err) {
    dispatch({
      type: POLL_RESULT_FAILURE
    });
  }
};


export const update_jenkins = (name, hostname, port, ssl, certificatecheck) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true,
    name,
    hostname,
    port,
    ssl,
    certificatecheck,
  });

  try {
    const res = await axios.post('/servers/jenkins/', body, config);

    if (res.data.detail) {
      dispatch({
        type: UPDATE_JENKINS_FAILURE
      });
    } else {
      dispatch({
        type: UPDATE_JENKINS_SUCCESS
      });
      dispatch(load_jenkins());
    }
  } catch (err) {
    dispatch({
      type: UPDATE_JENKINS_FAILURE
    });
  }
};


export const delete_jenkins = (jenkinsid) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true,
  });


  const url = "/servers/jenkins/" + jenkinsid + "/"

  try {
    const res = await axios.delete(url, config, body);

    if ( res.status >= 200 && res.status < 300) {
      dispatch({
        type: DELETE_JENKINS_SUCCESS
      });
      dispatch(load_jenkins());
    } else {
      dispatch({
        type: DELETE_JENKINS_FAILURE
      });
    }
  } catch (err) {
    dispatch({
      type: DELETE_JENKINS_FAILURE
    });
  }
};

export const update_job = (name, jenkins) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true,
    name,
    jenkins,
  });

  try {
    const res = await axios.post("/servers/jobs/", body, config);

    if (res.data.detail) {
      dispatch({
        type: UPDATE_JENKINS_JOB_FAILURE
      });
    } else {
      dispatch({
        type: UPDATE_JENKINS_JOB_SUCCESS
      });
      dispatch(load_jobs());
    }
  } catch (err) {
    dispatch({
      type: UPDATE_JENKINS_JOB_FAILURE
    });
  }
};


export const delete_job = (jobid) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true,
  });


  const url = "/servers/jobs/" + jobid + "/"

  try {
    const res = await axios.delete(url, config, body);

    if ( res.status >= 200 && res.status < 300) {
      dispatch({
        type: DELETE_JENKINS_JOB_SUCCESS
      });
      dispatch(load_jobs());
    } else {
      dispatch({
        type: DELETE_JENKINS_JOB_FAILURE
      });
    }
  } catch (err) {
    dispatch({
      type: DELETE_JENKINS_JOB_FAILURE
    });
  }
};
