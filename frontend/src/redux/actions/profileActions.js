import Cookies from 'js-cookie';
import axios from 'axios';
import {
  LOAD_USER_PROFILE_SUCCESS,
  LOAD_USER_PROFILE_FAILURE,
  UPDATE_USER_PROFILE_SUCCESS,
  UPDATE_USER_PROFILE_FAILURE,
  LOGIN_KNOX_SUCCESS,
  LOGIN_KNOX_FAILURE,
  UPDATE_USER_TOKEN_SUCCESS,
  UPDATE_USER_TOKEN_FAILURE,
  AUTHENTICATED_FAILURE,
  AUTHENTICATED_SUCCESS,
} from "../constants";

export const load_user = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  try {
    const res = await axios.get('/profile/user', config);

    if (res.data.error) {
      dispatch({
        type: LOAD_USER_PROFILE_FAILURE
      });
    } else {
      dispatch({
        type: LOAD_USER_PROFILE_SUCCESS,
        payload: res.data
      });
    }
  } catch (err) {
      dispatch({
        type: LOAD_USER_PROFILE_FAILURE
      });
  }
};

export const update_profile = (first_name, last_name, email, phone, city, province,
  country) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true,
    first_name,
    last_name,
    email,
    phone,
    city,
    province,
    country,
  });

  try {
    const res = await axios.put('/profile/update', body, config);

    if (res.data.profile && res.data.username) {
      dispatch({
        type: UPDATE_USER_PROFILE_SUCCESS,
        payload: res.data
      });
    } else {
      dispatch({
        type: UPDATE_USER_PROFILE_FAILURE
      });
    }
  } catch (err) {
    dispatch({
      type: UPDATE_USER_PROFILE_FAILURE
    });
  }
};

export const login_knox = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true
  });

  try {
    const profile = await axios.get('/profile/user', config);
      if (profile.data.error) {
        dispatch({
          type: LOAD_USER_PROFILE_FAILURE
        });
      } else if (!profile.data.profile.portal_knox_token) {
          dispatch({
            type: LOAD_USER_PROFILE_SUCCESS,
            payload: profile.data
          });
          const res = await axios.post('/accounts/loginknox', body, config);
          if (res.data.token) {
            dispatch(update_token(res.data.token));
            dispatch({
              type: LOGIN_KNOX_SUCCESS,
              payload: res.data
            });
          } else {
            dispatch({
              type: LOGIN_KNOX_FAILURE
            });
          }
      } else {
        dispatch({
          type: LOAD_USER_PROFILE_SUCCESS,
          payload: profile.data
        });
      }
  } catch (err) {
    dispatch({
      type: LOAD_USER_PROFILE_FAILURE
    });
  }
};

export const update_token = (portal_knox_token) => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  };

  const body = JSON.stringify({
    'withCredential': true,
    portal_knox_token,
  });

  try {
    const res = await axios.put('/profile/token', body, config);

    if (res.data.profile && res.data.username) {
      dispatch({
        type: UPDATE_USER_TOKEN_SUCCESS,
        payload: res.data
      });
    } else {
      dispatch({
        type: UPDATE_USER_TOKEN_FAILURE
      });
    }
  } catch (err) {
    dispatch({
      type: UPDATE_USER_TOKEN_FAILURE
    });
  }
};

export const checkAuthenticated = () => async dispatch => {
  const config = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  try {
    const res = await axios.get('/accounts/authenticated', config);

    if (res.data.error || res.data.isAuthenticated === 'error') {
      dispatch({
        type: AUTHENTICATED_FAILURE,
        payload: false
      });
    }
    else if (res.data.isAuthenticated === 'success') {
      dispatch({
        type: AUTHENTICATED_SUCCESS,
        payload: true
      });
    }
    else {
      dispatch({
        type: AUTHENTICATED_FAILURE,
        payload: false
      });
    }
  } catch (err) {
    dispatch({
      type: AUTHENTICATED_FAILURE,
      payload: false
    });
  }
};
