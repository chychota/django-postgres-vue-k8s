import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({ render: Render, isAuthenticated, ...rest }) => (
    <Route
        {...rest}
        render={props => isAuthenticated ? <Render {...props} /> : <Redirect to='/auth/sign-in' />}
    />
);

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, {})(PrivateRoute);
