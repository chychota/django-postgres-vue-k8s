import React, { useEffect } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { connect } from "react-redux";
import {
  dashboard as dashboardRoutes,
  page as pageRoutes
} from "./index";

import DashboardLayout from "../layouts/Dashboard";
import AuthLayout from "../layouts/Auth";
import Page404 from "../pages/auth/Page404";
import { checkAuthenticated} from "../redux/actions/authActions";
import { load_user } from "../redux/actions/profileActions";
import { load_jenkins, load_es, load_jobs} from "../redux/actions/serverActions";
import { load_jenkins_keys, load_testlink_keys } from "../redux/actions/keyActions";
import PrivateRoute from "./PrivateRoute";


const childRoutes = (Layout, routes) =>
  routes.map(({ children, path, component: Component }, index) =>
    children ? (
      // Route item with children
      children.map(({ path, component: Component }, index) => (
        <Route
          key={index}
          path={path}
          exact
          render={props => (
            <Layout>
              <Component {...props} />
            </Layout>
          )}
        />
      ))
    ) : (
      // Route item without children
      <Route
        key={index}
        path={path}
        exact
        render={props => (
          <Layout>
            <Component {...props} />
          </Layout>
        )}
      />
    )
  );

const childPrivateRoutes = (Layout, routes) =>
  routes.map(({ children, path, component: Component }, index) =>
    children ? (
      // Route item with children
      children.map(({ path, component: Component }, index) => (
        <PrivateRoute
          key={index}
          path={path}
          exact
          render={props => (
            <Layout>
              <Component {...props} />
            </Layout>
          )}
        />
      ))
    ) : (
      // Route item without children
      <PrivateRoute
        key={index}
        path={path}
        exact
        render={props => (
          <Layout>
            <Component {...props} />
          </Layout>
        )}
      />
    )
  );

const Routes = ({ checkAuthenticated , load_user, load_jenkins, load_es, load_jenkins_keys, load_testlink_keys, load_jobs}) => {
  useEffect(() => {
    checkAuthenticated();
    load_user();
    load_jenkins();
    load_es();
    load_jenkins_keys();
    load_testlink_keys();
    load_jobs();
  });
  return (
    <Router>
      <Switch>
        {childPrivateRoutes(DashboardLayout, dashboardRoutes)}
        {childRoutes(AuthLayout, pageRoutes)}
        <Route
          render={() => (
            <AuthLayout>
              <Page404/>
            </AuthLayout>
          )}
        />
      </Switch>
    </Router>
  )
};

export default connect(null, { checkAuthenticated, load_user, load_jenkins, load_es, load_jenkins_keys, load_testlink_keys, load_jobs })(Routes);
