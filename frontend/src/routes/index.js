import async from "../components/Async";

import {
  Home as HomeIcon,
  User as UserIcon,
  Users as UsersIcon,
  UserPlus as UserPlusIcon,
  Box as BoxIcon,
  Search as SearchIcon,
  Octagon as OctagonIcon
} from "react-feather";

// Auth
import SignIn from "../pages/auth/SignIn";
import SignUp from "../pages/auth/SignUp";

//Dashboard
const Dashboard = async(() => import("../pages/pages/Dashboard"));

//Profile
const Profile = async(() => import("../pages/profile/Profile"));
const Settings = async(() => import("../pages/profile/Settings"));

//Administration
const Flower = async(() => import("../pages/admin/Flower"));
const RedisCommander = async(() => import("../pages/admin/RedisCommander"));
const Jenkins = async(() => import("../pages/admin/Jenkins"));

//DevOps
const RunJenkinsJob = async(() => import("../pages/devops/RunJenkinsJob"));

//MVP
const QueryES = async(() => import("../pages/mvp/QueryES"));

//Whitelabel
const EKSDeploy = async(() => import("../pages/whitelabel/eks/Deploy"));
const EKSDispose = async(() => import("../pages/whitelabel/eks/Dispose"));
const MonitoringDeploy = async(() => import("../pages/whitelabel/monitoring/Deploy"));
const MonitoringDispose = async(() => import("../pages/whitelabel/monitoring/Dispose"));

const dashboardRoutes = {
  path: "/",
  name: "Dashboard",
  icon: HomeIcon,
  component: Dashboard,
  displayInSidebar: true,
  isAdmin: false,
  children: null
};

const userRoutes = {
  path: "/user/",
  name: "Personal",
  icon: UserIcon,
  displayInSidebar: false,
  isAdmin: false,
  children: [
    {
      path: "/user/profile",
      name: "Profile",
      component: Profile
    },
    {
      path: "/user/settings",
      name: "Edit Profile",
      component: Settings
    }
  ]
};

const authRoutes = {
  path: "/auth",
  name: "Auth",
  displayInSidebar: false,
  isAdmin: false,
  icon: UsersIcon,
  children: [
    {
      path: "/auth/sign-in",
      name: "Sign In",
      component: SignIn
    },
    {
      path: "/auth/sign-up",
      name: "Sign Up",
      component: SignUp
    }
  ]
};

const adminRoutes = {
  name: "Administration",
  icon: UserPlusIcon,
  isAdmin: true,
  displayInSidebar: true,
  children: [
    {
      path: "/admin",
      name: "Django Admin",
      component: () => {
        window.location.replace('/admin');
        return null;
      }
    },
    {
      path: "/portaladmin/flower",
      name: "Flower",
      component: Flower
    },
    {
      path: "/portaladmin/redis",
      name: "Redis Commander",
      component: RedisCommander
    },
    {
      path: "/portaladmin/pgadmin",
      name: "pgAdmin",
      component: () => {
        window.location.replace(process.env.REACT_APP_PGADMIN4_URL);
        return null;
      }
    },
    {
      path: "/portaladmin/jenkins",
      name: "Manage Jenkins",
      component: Jenkins
    },
  ]
};

// DevOps Routes
const devopsRoutes = {
  path: "/devops",
  name: "DevOps",
  displayInSidebar: true,
  isAdmin: false,
  icon: BoxIcon,
  children: [
    {
      path: "/devops/run",
      name: "Run Jenkins Job",
      component: RunJenkinsJob
    },
    {
      path: "/swagger",
      name: "API swagger",
      component: () => {
        window.location.replace('/swagger');
        return null;
      }
    },
  ]
};

// MVP Routes
const mvpRoutes = {
  path: "/mvp",
  name: "MVP",
  displayInSidebar: true,
  isAdmin: false,
  icon: SearchIcon,
  children: [
    {
      path: "/mvp/query",
      name: "Query ElasticSearch",
      component: QueryES
    }
  ]
};

// Whitelabel Routes
const whitelabelRoutes = {
  path: "/whitelabel",
  name: "White Label",
  displayInSidebar: true,
  isAdmin: false,
  icon: OctagonIcon,
  children: [
    {
      path: "/whitelabel/eks/deploy",
      name: "EKS Deploy",
      component: EKSDeploy
    },
    {
      path: "/whitelabel/eks/dispose",
      name: "EKS Dispose",
      component: EKSDispose
    },
    {
      path: "/whitelabel/monitoring/deploy",
      name: "Monitoring Deploy",
      component: MonitoringDeploy
    },
    {
      path: "/whitelabel/monitoring/dispose",
      name: "Monitoring Dispose",
      component: MonitoringDispose
    }
  ]
};

// Dashboard specific routes
export const dashboard = [
  dashboardRoutes,
  userRoutes,
  adminRoutes,
  devopsRoutes,
  mvpRoutes,
  whitelabelRoutes,
];

// Auth specific routes
export const page = [authRoutes];

// All routes
export default [
  dashboardRoutes,
  userRoutes,
  authRoutes,
  adminRoutes,
  devopsRoutes,
  mvpRoutes,
  whitelabelRoutes,
];
