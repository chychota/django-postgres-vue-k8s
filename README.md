# Project Documentation

## Contents
1. [Tools, technologies, and frameworks](#tools-technologies-frameworks)
2. [Ramping up to do portal development work](#ramping-up)
3. [Project directory structure](#directory-structure)
4. [Local development workflow](#local-development-workflow)
5. [How to run Robot Framework tests locally](#run-robot-tests-locally)
6. [How to run full test suite](#run-full-test-suite)

## Tools, technologies, and frameworks <a name="tools-technologies-frameworks"></a>

The portal code uses the following tools, technologies, and frameworks:

* [Django](https://www.djangoproject.com/)
* [React](https://reactjs.org/)
* [Redis](https://redis.io/)
* [Redis Commander](https://joeferner.github.io/redis-commander/)
* [PostgreSQL](https://www.nginx.com/)
* [pgAdmin](https://www.pgadmin.org/)
* [Celery](http://celeryproject.org/)
* [Flower](https://github.com/mher/flower)
* [NGINX](https://www.nginx.com/)
* [Robot Framework](https://robotframework.org/)
* [Selenium](https://www.selenium.dev/)
* [Docker](https://www.docker.com/)
* [Kubernetes](https://kubernetes.io/)
* [Helm](https://helm.sh/)

The React frontend was built using the [AppStack](https://appstack-react.bootlab.io/)
React GUI package.

## Ramping up to do portal development work <a name="ramping-up">

Before beginning development work on the portal, it is recommended you read the
following:

* [Django tutorial](https://docs.djangoproject.com/en/3.1/intro/tutorial01/)
* [Django REST framework tutorial](https://www.django-rest-framework.org/tutorial/1-serialization/)
* [React tutorial](https://reactjs.org/tutorial/tutorial.html)

It might also be helpful to read up on ES6 syntax if you know JavaScript but are not
familiar with ES6 syntax.

## Project directory structure <a name="directory-structure">

This project has the following top-level directories:

* `backend` - contains the backend code written in Django
* `frontend` - contians the frontend code written in React
* `helm` - contains the helm chart
* `nginx` - contains NGINX configuration
* `robot` - contains Robot Framework tests, including Selenium tests
* `scripts` - contains scripts

## Local development workflow <a name="local-development-workflow">

Both the backend and frontend are configured to hot reload the code changes on
the fly in a local development environment.

To spin up a local portal, `cd` to the root of the portal's directory, and then
run
```bash
docker-compose -f docker-compose.dev.yml up -d
```

This will start the portal. Once all Docker containers have finished initializing
(which usually takes about a minute, but may take more or less time depending on
how powerful your local machine is), the portal frontend can be accessed in your
web browser at http://localhost/.

To tear down the local portal when you are finished developing, or when you
made a change that cannot be picked up the hot reloading (like, for example,
adding a new dependency), run
```bash
docker-compose down --remove-orphans
```

At any time during development, the logs of any container can be watched. For
example, the `backend` container can be watched with the command
```bash
docker logs -f backend
```

## How to run Robot Framework tests locally <a name="run-robot-tests-locally">

It is useful to be able to run the Robot tests locally, especially when you are
writing new Robot tests. This section explains how to run the Robot tests on a
local development machine.

**Note**: The Robot tests passing locally does **not** guarantee the tests will
pass with the merge request pipeline, since the kubernetes environment will not
be identical to your local development environment. Sometimes it is necessary
to deploy the portal to a test kubernetes namespace and debug why the tests
are not passing with the kubernetes environment.

To run the Robot Framework tests locally, `cd` to the root directory of the
project, and then run

```bash
scripts/dev_run_selenium_tests.sh
```

To run tests from only a single file, instead run

```bash
scripts/dev_run_selenium_tests.sh <robot-file>.robot
```

For example, to run just the login tests:

```bash
scripts/dev_run_selenium_tests.sh login.robot
```

## How to run full test suite <a name="run-full-test-suite">

To run the entire test suite, including linting, Django tests, and Robot
Framework tests, go to [run_portal_full-test-suite](https://jenkins.ion-sbu.dyn.nesc.nokia.net/view/Run/job/run_portal_full-test-suite/)
in Jenkins, click `Build with Parameters`, set the `PortalBranch`
parameter to the name of your branch, and then press `Build`.

This may be useful if you want to test the full test suite passes before you
create a merge request.
